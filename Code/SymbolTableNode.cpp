#include "SymbolTableNode.h"

SymbolTableNode::SymbolTableNode(SymbolTableNode* parent)
{
	this->parent = parent;
	this->currChild = -1; // thats because when moving to a child we increase the index, -1 represents the state before we entered the first child scope
}

SymbolTableNode* SymbolTableNode::getParent()
{
	return this->parent;
}

Symbol* SymbolTableNode::getSymbolFromTable(string identifierName)
{
	Symbol* result = nullptr;
	if(symbolTable.find(identifierName) != symbolTable.end())
		result = this->symbolTable[identifierName];
	return result;
}

SymbolTableNode* SymbolTableNode::getCurrChildObject()
{
	return children[currChild];
}

int SymbolTableNode::getCurrChildIndex()
{
	return currChild;
}

vector<SymbolTableNode*>* SymbolTableNode::getChildren()
{
	return &(this->children);
}

void SymbolTableNode::insertToSymbolTable(string identifierName, identifierType type, int posInFunc, int posInParams)
{
	this->symbolTable[identifierName] = new Symbol(type, identifierName, posInFunc, posInParams);
	amountOfSymbols++;
}

void SymbolTableNode::setCurrChildIndex(int index)
{
	this->currChild = index;
}

void SymbolTableNode::printSymbolTable()
{
	for (const auto& pair : symbolTable) {
		std::cout << "idName: " << pair.first << ", type: " << pair.second->getType() << std::endl;
	}
}

int SymbolTableNode::getAmountOfSymbols()
{
	return amountOfSymbols;
}