#include "FileDropTarget.h"
#include <wx/textfile.h>
#include <wx/tokenzr.h>

FileDropTarget::FileDropTarget(wxTextCtrl* textCtrl)
    : m_textCtrl(textCtrl) {}

bool FileDropTarget::OnDropFiles(wxCoord, wxCoord, const wxArrayString& filenames)
{
    if (filenames.empty())
        return false;

    wxString filepath = filenames[0]; // Assuming only one file is dropped

    // Open the file with wxTextFile
    wxTextFile file;
    if (file.Open(filepath)) {
        wxTextCtrl* textCtrl = m_textCtrl; // Retrieve the text control
        textCtrl->Clear(); // Clear existing content

        // Read the file line by line
        for (size_t i = 0; i < file.GetLineCount(); ++i) {
            wxString line = file.GetLine(i);
            textCtrl->AppendText(line + wxT("\n")); // Append each line to the text control
        }

        file.Close();
        return true;
    }
    return false;
}
