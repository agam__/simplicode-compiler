#include "Action.h"

Action::Action(ActionType at, int srn)
{
	this->actionType = at;
	this->shift_reduce_number = srn;
	this->semanticCheck = -1;
}

ActionType Action::getActionType()
{
	return this->actionType;
}

int Action::getStateOrRule()
{
	return this->shift_reduce_number;
}

int Action::getSemanticCheck()
{
	return this->semanticCheck;
}

void Action::setSemanticCheck(int SemanticCheck)
{
	this->semanticCheck = SemanticCheck;
}