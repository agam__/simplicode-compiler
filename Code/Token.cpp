#include "Token.h"

Token::Token(TokenType tt, string data) {
	tokenType = tt;
	this->data = data;
}

TokenType Token::getType()
{
	return this->tokenType;
}

string Token::getTokenData()
{
	return this->data;
}