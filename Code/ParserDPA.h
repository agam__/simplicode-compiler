#pragma once
#include "Action.h"
#include <iostream>
#include <vector>

using std::vector;

/// <summary>
/// this class represents the parsing table - the Push-Down Automata of the parser 
/// </summary>
class ParserDPA
{
public:
	// constructor
	ParserDPA();
	// function to access a cell on the action table using a row (state) and a column (TokenType)
	Action getAction(int state, TokenType tt);
	// function to access a cell on the goto table using a row (state) and a column (NonTerminalType)
	int getGoto(int state, NonTerminalType ntt);
	// functions to print the tables - used for debugging
	void printGotoTable();
	void printActionTable();
private:
	vector<vector<Action>> actionTable;
	vector<vector<int>> gotoTable;
	// initializes the action table
	void initActionTable();
	// initializes the goto table
	void initGotoTable();
	// function to create the action table from a csv
	std::vector<std::vector<Action>> createActionMatrixFromCSV(const std::string& filename);
	// function to create the goto table from a csv
	std::vector<std::vector<int>> createGotoMatrixFromCSV(const std::string& filename);
	
	//converts a string to Action type - for example "acc" -> Accept
	ActionType stringToActionType(const std::string& str);
};

