#pragma once
#ifndef SEMANTICANALYZER_H
#define SEMANTICANALYZER_H
class Parser;
#include<map>
#include <iostream>
#include <stack>
#include "ScopeTree.h"
#include "TreeNode.h"
#include "ErrorHandler.h"
using std::map;
using std::stack;
using std::pair;

class SemanticAnalyzer
{
public:
	// constructor
	SemanticAnalyzer(Parser* parser, ErrorHandler* errorHandler, ScopeTree* scopeTree);
	// executes a semantic function using the semanticFuncs hash table
	void executeSemanticFunction(int semanticCheckNumber);
	// getter function for the announcedInMain field
	int getAnnouncedInMain();
private:
	ErrorHandler* semanticErrorHandler;
	Parser* parserPointer;
	ScopeTree* scopeTree;
	stack<pair<TreeNode*, int>> semanticStack;
	map<SemanticCheck, void(SemanticAnalyzer::*)()> semanticFuncs;
	int announcedInMain;

	
	void initSemanticFunctions();

	// helper functions
	identifierType TokenType_To_IdentifierType(TokenType tt);
	void recursiveConvertTreeToParameters(TreeNode* paramsRoot, map<string, Symbol*>* pMap, vector<Symbol*>* pVector);
	identifierType getIdentifierTypeFromTreeNode(TreeNode* t);
	bool checkFuncCallTree(TreeNode* funcCallRoot);
	bool recursiveCheckFuncParameters(int currIndex, TreeNode* callablesTree, Function* func);
	int recursiveGetCallablesSize(TreeNode* callablesTree);

	// functions in semanticFuncs
	void CheckAnnounce();
	void CheckAssignType();
	void CheckFuncCall();
	void CheckFuncCallInFactor();
	void CheckIdentifierInFactor();
	void CheckComparison();
	void CheckOut();
	void CheckIn();
	void CheckRet();
	void AddFunc();
	void EnterScope();
	void ExitScope();
	void ExitFunc();
};
#endif