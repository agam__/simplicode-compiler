#include "Compiler.h"

Compiler::Compiler(std::string srcCode)
{
	this->errorHandler = new ErrorHandler();
	this->lexer = new Lexer(srcCode, errorHandler);
	this->scopeTree = new ScopeTree();
	this->parser = new Parser(errorHandler, scopeTree);
}

string Compiler::compile()
{
	std::vector<Token> tokens = lexer->tokenize();
	if(tokens.empty())
		return "Compilaton error - check log";
	parser->setTokens(lexer->tokenize());
	TreeNode* AST = parser->parse();
	if (AST != nullptr && errorHandler->getAmountOfSemanticErrors() == 0)// checking if can generate code
	{
		this->codeGenerator = new CodeGenerator(AST, scopeTree, parser->getAnnouncedInMain());
		try {
			return codeGenerator->generate();
		}
		catch (std::exception e)
		{
			return e.what();
		}
	}
	return "Compilaton error - check log";
}