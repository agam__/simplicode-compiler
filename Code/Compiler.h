#pragma once
#include "Lexer.h"
#include "Parser.h"
#include "CodeGenerator.h"

/// <summary>
/// main class representing the compiler, contains all the components of the compilation proccess
/// INPUT: string (srcCode) 
/// OUTPUT: string (destCode)
/// </summary>
class Compiler
{
private:
	Lexer* lexer;
	Parser* parser;
	ErrorHandler* errorHandler;
	CodeGenerator* codeGenerator;
	ScopeTree* scopeTree;
public:
	//constructor for the compiler 
	Compiler(std::string srcCode);
	//function for starting the compilation process on the source code, returns the assmebly x86 destination code string
	string compile();
};
