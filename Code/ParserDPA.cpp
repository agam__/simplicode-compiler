#include "ParserDPA.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

ParserDPA::ParserDPA()
{
	initActionTable();
	initGotoTable();
}

// Function to convert string to ActionType
ActionType ParserDPA::stringToActionType(const std::string& str) {
	if (str == "s") return SHIFT;
	else if (str == "r") return REDUCE;
	else if (str == "acc") return ACCEPT;
	else return ERROR;
}


std::vector<std::vector<Action>> ParserDPA::createActionMatrixFromCSV(const std::string& filename) {
	std::ifstream file(filename);
	std::vector<std::vector<Action>> actionMatrix;

	std::string line;
	while (std::getline(file, line)) {
		std::vector<Action> row;
		std::istringstream iss(line);
		std::string cell;

		while (std::getline(iss, cell, ',')) {
			if (cell.empty()) {
				row.emplace_back(ERROR, 0);
			}
			else if (cell.substr(0, 1) == "s" || cell.substr(0, 1) == "r") {
				int num = std::stoi(cell.substr(1));
				row.emplace_back(stringToActionType(cell.substr(0, 1)), num);
			}
			else {
				row.emplace_back(stringToActionType(cell), 0);
			}
		}
		if (row.size() < 42)
			row.emplace_back(ERROR, 0);
		actionMatrix.push_back(row);
	}

	// Ensure that there are exactly 169 states by adding empty rows if needed
	while (actionMatrix.size() < 174) {
		std::vector<Action> emptyRow;
		for (size_t i = 0; i < actionMatrix[0].size(); ++i) {
			emptyRow.emplace_back(ERROR, 0);
		}
		actionMatrix.push_back(emptyRow);
	}
	return actionMatrix;
}

std::vector<std::vector<int>> ParserDPA::createGotoMatrixFromCSV(const std::string& filename) {
	std::ifstream file(filename);
	std::vector<std::vector<int>> gotoMatrix;

	std::string line;
	while (std::getline(file, line)) {
		std::vector<int> row;
		std::istringstream iss(line);
		std::string cell;

		row.push_back(-1);

		while (std::getline(iss, cell, ',')) {
			// Convert string to integer and add to the row
			if (cell.empty())
				row.push_back(-1); // Represent empty cell with -1
			else
				row.push_back(std::stoi(cell));
		}
		gotoMatrix.push_back(row);
	}

	// Ensure that there are exactly 174 states by adding empty rows if needed
	while (gotoMatrix.size() < 174) {
		std::vector<int> emptyRow;
		for (size_t i = 0; i < gotoMatrix[0].size(); ++i) {
			emptyRow.push_back(-1);
		}
		gotoMatrix.push_back(emptyRow);
	}

	return gotoMatrix;
}

void ParserDPA::initActionTable()
{
	actionTable = createActionMatrixFromCSV("actionTable.csv");
	//now adding semantic checks for relevant actions in the table
	// this spesific cells in the table is when we have a func TYPE/void identifier(PARAMETERS)
	actionTable[24][CLOSING_PARENTHESIS_TOKEN].setSemanticCheck(Add_Func);
	actionTable[21][CLOSING_PARENTHESIS_TOKEN].setSemanticCheck(Add_Func);
	
	//now adding that when reducing assign to statement, checking the assign - type and if identifier exist
	for (int i = 0; i < actionTable[44].size(); i++)
	{
		if (actionTable[44][i].getActionType() == REDUCE)
			actionTable[44][i].setSemanticCheck(Assign_Type_Check);
	}
	//now adding that when reducing to ANNOUNCE, checking the type assignment, and if there is already identifer with same name
	for (int i = 0; i < actionTable[62].size(); i++)
	{
		if (actionTable[62][i].getActionType() == REDUCE)
			actionTable[62][i].setSemanticCheck(Announce_Check);
	}
	//
	for (int i = 0; i < actionTable[111].size(); i++)
	{
		if (actionTable[111][i].getActionType() == REDUCE)
			actionTable[111][i].setSemanticCheck(Identifier_In_Factor_Check);
	}
	//
	for (int i = 0; i < actionTable[156].size(); i++)
	{
		if (actionTable[156][i].getActionType() == REDUCE)
			actionTable[156][i].setSemanticCheck(In_Check);
	}

	//
	for (int i = 0; i < actionTable[155].size(); i++)
	{
		if (actionTable[155][i].getActionType() == REDUCE)
			actionTable[155][i].setSemanticCheck(Out_Check);
	}
	//
	for (int i = 0; i < actionTable[81].size(); i++)
	{
		if (actionTable[81][i].getActionType() == REDUCE)
			actionTable[81][i].setSemanticCheck(Exit_Func);
	}
	for (int i = 0; i < actionTable[59].size(); i++)
	{
		if (actionTable[59][i].getActionType() == REDUCE)
			actionTable[59][i].setSemanticCheck(Exit_Func);
	}
	//
	for (int i = 0; i < actionTable[104].size(); i++)
	{
		if (actionTable[104][i].getActionType() == REDUCE)
			actionTable[104][i].setSemanticCheck(Ret_Check);
	}
	//
	for (int i = 0; i < actionTable[153].size(); i++)
	{
		if (actionTable[153][i].getActionType() == REDUCE)
			actionTable[153][i].setSemanticCheck(Comparison_Check);
	}
	//
	for (int i = 0; i < actionTable[110].size(); i++)
	{
		if (actionTable[110][i].getActionType() == REDUCE)
			actionTable[110][i].setSemanticCheck(Func_Call_In_Factor_Check);
	}
	//
	for (int i = 0; i < actionTable[61].size(); i++)
	{
		if (actionTable[61][i].getActionType() == REDUCE)
			actionTable[61][i].setSemanticCheck(Func_Call_Check);
	}

	//after shifting { need to enter scope
	for (int i = 0; i < actionTable.size(); i++)
		if (actionTable[i][OPENING_CURLY_BRACKETS_TOKEN].getActionType() == SHIFT)
			actionTable[i][OPENING_CURLY_BRACKETS_TOKEN].setSemanticCheck(Enter_Scope);
	//after shifting } need to exit scope
	for (int i = 0; i < actionTable.size(); i++)
		if(actionTable[i][CLOSING_CURLY_BRACKETS_TOKEN].getActionType() == SHIFT)
			actionTable[i][CLOSING_CURLY_BRACKETS_TOKEN].setSemanticCheck(Exit_Scope);
}

Action ParserDPA::getAction(int state, TokenType tt)
{
	return actionTable[state][tt];
}

int ParserDPA::getGoto(int state, NonTerminalType ntt)
{
	return gotoTable[state][ntt];
}


void ParserDPA::initGotoTable()
{
	gotoTable = createGotoMatrixFromCSV("gotoTable.csv");
}

void ParserDPA::printGotoTable()
{
	for (const auto& row : gotoTable) {
		for (const auto& val : row) {
			if (val == -1)
				std::cout << "   "; // Printing empty cell as space
			else
				std::cout << val << " ";
		}
		std::cout << std::endl;
	}
}

void ParserDPA::printActionTable()
{
	for (const auto& row : actionTable) {
		for (Action action : row) {
			switch (action.getActionType()) {
			case ERROR:
				std::cout << "Action(ERROR, 0) ";
				break;
			case SHIFT:
				std::cout << "Action(SHIFT, " << action.getStateOrRule() << ") ";
				break;
			case REDUCE:
				std::cout << "Action(REDUCE, " << action.getStateOrRule() << ") ";
				break;
			case ACCEPT:
				std::cout << "Action(ACCEPT, 0) ";
				break;
			}
		}
		std::cout << "\n\n" << std::endl;
	}
}


