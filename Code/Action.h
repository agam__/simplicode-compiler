#pragma once
#include "Helper.h"

/// <summary>
/// the purpose of this class is representing a single cell in the action table (part of the parsing table).
/// </summary>
class Action
{
public:
	//constructor function 
	Action(ActionType at, int srn);

	//getter function for the actionType field
	ActionType getActionType();

	//getter function for the shift_reduce_number, representing the number beside the action on the action table
	int getStateOrRule();

	//getter function for the semanticCheck field 
	int getSemanticCheck();

	//setter function for the semanticCheck field
	void setSemanticCheck(int SemanticCheck);
private:
	ActionType actionType;
	int shift_reduce_number;
	int semanticCheck;
};

