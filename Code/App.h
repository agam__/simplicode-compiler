#pragma once
#include <wx/app.h>
/// <summary>
/// this is the primary class representing the GUI, onInit runs the whole GUI
/// </summary>
class App : public wxApp
{
public:
	bool OnInit();
private:
};
