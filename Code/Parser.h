#pragma once
#ifndef PARSER_H
#define PARSER_H
class SemanticAnalyzer;
#include "Token.h"
#include "ParserDPA.h"
#include "ProductionRule.h"
#include "TreeNode.h"
#include "ErrorHandler.h"
#include <iostream>
#include <vector>
#include <stack>
#include "ScopeTree.h"

using std::vector;
using std::stack;
using std::pair;

/// <summary>
/// responsible for the syntax analysis proccess, using a Push-Down Automata (PDA) and the parsing algorithm.
/// the result of this proccess is the AST.
/// </summary>
class Parser
{
public:
	// constructor, initiates the parsing table, stack and production rules.
	Parser(ErrorHandler* errorHandler, ScopeTree* scopeTree);
	// setter for the tokens field
	void setTokens(vector<Token> tokens);
	// the main parsing algorithm, using the parser's components to parse the input tokens and return the AST (or an error).
	TreeNode* parse();
	// function to pointer to the stack, because the semantic needs it for semantic checks
	stack<pair<TreeNode*, int>>* getStack();
	// returns the announcedInMain value of the semanticPointer field
	int getAnnouncedInMain();
private:
	// shift action for the parsing algorithm
	void shift(int gotoState, Token* t);
	// reduce action for the parsing algorithm
	void reduce(int productionRule);
	//function to initialize the production rules vector
	void initProductionRules();

	vector<Token> tokens;
	vector<ProductionRule> productionRules;
	ParserDPA* parsingTable;
	stack<pair<TreeNode*, int>> parserStack;
	SemanticAnalyzer* semanticPointer;
	ErrorHandler* parserErrorHandler;
};
#endif