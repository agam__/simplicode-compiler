#pragma once
#include <wx/frame.h>

class MainFrame : public wxFrame
{
public:
	MainFrame(const wxString& title);
private:
	void onCompileButtonClicked(wxCommandEvent& event);
	void onRunButtonClicked(wxCommandEvent& event);
	wxDECLARE_EVENT_TABLE();
};

