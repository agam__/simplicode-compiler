#pragma once
#include "TreeNode.h"
#include "ScopeTree.h"
#include "Register.h"
using std::vector;

/// <summary>
/// this class uses the AST and Scope Tree from the previous components of the compiler, and generates code
/// in assembly x86 language.
/// </summary>
class CodeGenerator
{
public:
	//------------main general functions, used across the generating process
	//constructor for the Code Generator
	CodeGenerator(TreeNode* ast, ScopeTree* scopeTree, int announcedInMain);
	// main generating function - will return the final assembly code
	string generate();
private:
	//function used to write the actual assembly parts of the code segment
	void writeToDestStringCode(string line);
	//function used to write the actual assembly parts of the data segment
	void writeToDestStringData(string line);
	//used to allocate a free register from the registers vector and return a pointer to it
	Register* getRegister();
	//----------------------------------------------------------------------

	//------------functions for actual generating
	
	//used to generate the functions Tree Node in the AST
	void generateFunctions(TreeNode* funcs);
	//used to generate the main function
	void generateMain(TreeNode* statementsNode);
	// this will get FUNC treenode, and generate Assembly code for the function.
	void generateFunction(TreeNode* funcNode);
	// this will get STATEMENTS tree node, traverse recursively and use generateStatement for every statement
	void generateStatements(TreeNode* statementsNode);
	// this will get a node which is a child of STATEMENT, for example ASSIGN. and generate the assembly code for the statement.
	void generateStatement(TreeNode* statementNode);

	//---used to generate the different types of statements
	void generateAnnounce(TreeNode* announceNode);
	void generateDuring(TreeNode* duringNode);
	void generateIf(TreeNode* IfNode);
	void generateOut(TreeNode* outNode);
	void generateIn(TreeNode* inNode);
	void generateEndLine(TreeNode* endLineNode);

	void generateFuncCall(TreeNode* funcCallNode);
	void generateCallables(TreeNode* callables);
	void generateCallable(TreeNode* callable);

	void generateAssign(TreeNode* assignNode);
	void generateRet(TreeNode* retNode);
	//------------------------------------------------------

	//functions used to generate a boolean expression
	void generateBooleanExpression(TreeNode* expression);
	void generateBooleanTerm(TreeNode* boolTerm);
	void generateBooleanFactor(TreeNode* boolFactor);
	void generateComparison(TreeNode* comparison);
	void generateOperand(TreeNode* operand);

	//functions used to generate an arithmetic expression
	void generateExpression(TreeNode* expression);
	void generateTerm(TreeNode* term);
	void generateFactor(TreeNode* factor);

	//used to calculate offsets from the EBP and ESP registers
	int getSizeLocalVariablesBytesInStatements(TreeNode* statementsNode, int currPos);
	string getEbpOffsetString(Symbol* identifier);

	//functions to generate names for label/identifiers in the assembly code, used to avoid conflicts in label names
	string generateLabelString();
	string generateLitString();

	//fields
	TreeNode* AST;
	vector<Register> registers;
	ScopeTree* scopeTree;
	string destStringData;
	string destStringCode;
	int sizeVariablesMain;
	map<NonTerminalType, void(CodeGenerator::*)(TreeNode*)> generateStatementMap;
	map<TokenType, string> convertTokenTypeToJmpMap;
	int currLabel;
	int currLitString;
};

