#include "SemanticAnalyzer.h"
#include "Parser.h"
#include <cstring>

SemanticAnalyzer::SemanticAnalyzer(Parser* parser, ErrorHandler* errorHandler, ScopeTree* scopeTree)
{
	parserPointer = parser;
	this->semanticErrorHandler = errorHandler;
	this->scopeTree = scopeTree;
	initSemanticFunctions();
}

void SemanticAnalyzer::executeSemanticFunction(int semanticCheckNumber)
{
	auto func = semanticFuncs.find(static_cast<SemanticCheck>(semanticCheckNumber));
	(this->*(func->second))();
}

void SemanticAnalyzer::initSemanticFunctions()
{
	semanticFuncs[Announce_Check] = &SemanticAnalyzer::CheckAnnounce;
	semanticFuncs[Assign_Type_Check] = &SemanticAnalyzer::CheckAssignType;
	semanticFuncs[Func_Call_Check] = &SemanticAnalyzer::CheckFuncCall;
	semanticFuncs[Out_Check] = &SemanticAnalyzer::CheckOut;
	semanticFuncs[In_Check] = &SemanticAnalyzer::CheckIn;
	semanticFuncs[Ret_Check] = &SemanticAnalyzer::CheckRet;
	semanticFuncs[Add_Func] = &SemanticAnalyzer::AddFunc;
	semanticFuncs[Enter_Scope] = &SemanticAnalyzer::EnterScope;
	semanticFuncs[Exit_Scope] = &SemanticAnalyzer::ExitScope;
	semanticFuncs[Func_Call_In_Factor_Check] = &SemanticAnalyzer::CheckFuncCallInFactor;
	semanticFuncs[Identifier_In_Factor_Check] = &SemanticAnalyzer::CheckIdentifierInFactor;
	semanticFuncs[Comparison_Check] = &SemanticAnalyzer::CheckComparison;
	semanticFuncs[Exit_Func] = &SemanticAnalyzer::ExitFunc;
}


void SemanticAnalyzer::CheckAnnounce()
{
	string errorMsg = "";
	bool add = true;
	TreeNode* AnnounceNode = parserPointer->getStack()->top().first;
	TokenType typeAnnounced = AnnounceNode->getChild(0)->getChild(0)->getToken()->getType();
	Token* identifier = AnnounceNode->getChild(1)->getChild(0)->getToken();
	Function* currFunc = scopeTree->getCurrFunc();
	if (scopeTree->checkForIdentifier(identifier) != nullptr)
	{
		errorMsg += "variable name already used\n";
		add = false;
	}
	if (TokenType_To_IdentifierType(typeAnnounced) != getIdentifierTypeFromTreeNode(AnnounceNode->getChild(1)->getChild(2)))
	{
		errorMsg += "type mismatch\n";
		add = false;
	}
	try {
		if (!add)
			throw std::exception(errorMsg.c_str());
		if(!currFunc)
			this->scopeTree->addIdentifier(identifier->getTokenData(), TokenType_To_IdentifierType(typeAnnounced), announcedInMain++, -1);
		else {
			this->scopeTree->addIdentifier(identifier->getTokenData(), TokenType_To_IdentifierType(typeAnnounced), currFunc->getCurrAnnounce(), -1);
			currFunc->increaseCurrAnnounce();
		}
	}
	catch (std::exception e)
	{
		semanticErrorHandler->handleSemanticError(e);
	}
}

identifierType SemanticAnalyzer::TokenType_To_IdentifierType(TokenType tt)
{
	if (tt == VOID)
		return VOID_TYPE;
	if (tt == INT || tt == INT_LITERAL || tt == ARITHMETIC_TOKEN)
		return INT_TYPE;
	if (tt == BOOL || tt == TRUE || tt == BOOLEAN_TOKEN || tt == FALSE)
		return BOOL_TYPE;
	if (tt == STRING || tt == STRING_LITERAL)
		return STRING_TYPE;
}

void SemanticAnalyzer::CheckAssignType()// will be used after reducing by STATEMENT -> ASSIGN
{
	TreeNode* AssignNode = parserPointer->getStack()->top().first->getChild(0);
	Token* identifier = AssignNode->getChild(0)->getToken();
	Symbol* identifierSymbol;
	try {
		if ((identifierSymbol = scopeTree->checkForIdentifier(identifier)) == nullptr)
			throw std::exception("Trying to assign in non existent identifier\n");
		else if (getIdentifierTypeFromTreeNode(AssignNode->getChild(2)) != identifierSymbol->getType())
			throw std::exception("Trying to assign incorrect type to identifier\n");
	}
	catch (std::exception e)
	{
		semanticErrorHandler->handleSemanticError(e);
	}
}

void SemanticAnalyzer::AddFunc()
{	
	identifierType retType;

	map<string, Symbol*>* parameters = new map<string, Symbol*>();
	vector<Symbol*>* parametersVector = new vector<Symbol*>();
	this->semanticStack = *(this->parserPointer->getStack());
	semanticStack.pop();// get rid of )
	TreeNode* parametersTree = semanticStack.top().first;
	recursiveConvertTreeToParameters(parametersTree, parameters, parametersVector);
	semanticStack.pop();// get rid of parameters
	semanticStack.pop();// get rid of (
	string funcName = semanticStack.top().first->getToken()->getTokenData();
	semanticStack.pop();// get rid of function name
	if(semanticStack.top().first->getToken() != nullptr)
		retType = TokenType_To_IdentifierType(semanticStack.top().first->getToken()->getType());
	else
		retType = TokenType_To_IdentifierType(semanticStack.top().first->getChild(0)->getToken()->getType());
	Function func = Function(funcName, parameters, retType, parametersVector);
	scopeTree->addFunction(funcName, func);
	scopeTree->setCurrFunc(scopeTree->getFunction(funcName));
	scopeTree->getCurrFunc()->setName(funcName);
	scopeTree->getCurrFunc()->setParametersVector(parametersVector);
	semanticStack = stack<pair<TreeNode*, int>>();// reseting the semantic stack
}

void SemanticAnalyzer::recursiveConvertTreeToParameters(TreeNode* paramsRoot, map<string, Symbol*>* pMap, vector<Symbol*>* pVector)
{
	string paramName = paramsRoot->getChild(0)->getChild(1)->getToken()->getTokenData();
	identifierType paramType = TokenType_To_IdentifierType(paramsRoot->getChild(0)->getChild(0)->getChild(0)->getToken()->getType());
	int posInParams = pVector->size();
	pMap->emplace(paramName, new Symbol(paramType, paramName, -1, posInParams));// putting -1 in the posInFunc because the symbol is a parameter
	pVector->emplace_back(new Symbol(paramType, paramName, -1, posInParams));//  """"
	if (paramsRoot->getChildren().size() < 3)
		return;
	recursiveConvertTreeToParameters(paramsRoot->getChild(2), pMap, pVector);
}

void SemanticAnalyzer::EnterScope()
{
	scopeTree->addScope();
	scopeTree->moveDown();
}

void SemanticAnalyzer::ExitScope()
{
	//scopeTree.printCurrScope();
	scopeTree->moveUp();
}

void SemanticAnalyzer::CheckIdentifierInFactor()
{
	Symbol* identifier = scopeTree->checkForIdentifier(parserPointer->getStack()->top().first->getChild(0)->getToken());
	try {
		if (identifier == nullptr)
			throw std::exception("Use of non existent variable inside arithmetic expression\n");
		else if (identifier->getType() != INT_TYPE)
			throw std::exception("Use of non int variable inside arithmetic expression\n");
	}
	catch (std::exception e)
	{
		semanticErrorHandler->handleSemanticError(e);
	}

}

void SemanticAnalyzer::CheckIn()
{
	Symbol* identifier = scopeTree->checkForIdentifier(parserPointer->getStack()->top().first->getChild(2)->getToken());
	try {
		if (identifier == nullptr)
		{
			throw std::exception("Use of non existent variable inside in function\n");
		}
		else if (identifier->getType() == BOOL_TYPE)
		{
			throw std::exception("Cannot accept input into boolean variable\n");
		}
	}
	catch (std::exception e)
	{
		semanticErrorHandler->handleSemanticError(e);
	}
}


void SemanticAnalyzer::CheckOut()
{
	Symbol* identifier;
	Token* printedToken = parserPointer->getStack()->top().first->getChild(2)->getChild(0)->getToken();
	try {
		if (printedToken->getType() == IDENTIFIER)
		{
			identifier = scopeTree->checkForIdentifier(printedToken);
			if (identifier == nullptr)
			{
				throw std::exception("Cannot print non existent identifier\n");
			}
			else if (identifier->getType() == BOOL_TYPE)
			{
				throw std::exception("Cannot print boolean identifier\n");
			}
		}
	}
	catch (std::exception e)
	{
		semanticErrorHandler->handleSemanticError(e);
	}
}

//after reducing to FUNC we cannot longer use the functions parameters.
void SemanticAnalyzer::ExitFunc()
{
	try {
		if (scopeTree->getCurrFunc()->getIsReturnedGood() == false)
			throw std::exception("Function doesn't return type or doesn't return correct type!\n");
	}
	catch (std::exception e)
	{
		semanticErrorHandler->handleSemanticError(e);
	}
	scopeTree->setCurrFunc(nullptr);
}

//this function checks both operands, to see of their types match, also checks if the identifiers/function call in the comparison
//actually exist and are valid.
void SemanticAnalyzer::CheckComparison()
{
	TreeNode* comparisonNode = parserPointer->getStack()->top().first;
	TreeNode* op1 = comparisonNode->getChild(0)->getChild(0);
	TreeNode* op2 = comparisonNode->getChild(2)->getChild(0);
	identifierType typeOp1;
	identifierType typeOp2;
	Symbol* identifier;
	try {
		if (op1->getNonTerminalType() == _FUNC_CALL)
		{
			if (!checkFuncCallTree(op1))
				throw std::exception("Function call in comparison is invalid!\n");
			typeOp1 = scopeTree->getFunction(op1->getChild(1)->getToken()->getTokenData())->getReturnType();
		}
		if (op2->getNonTerminalType() == _FUNC_CALL)
		{
			if (!checkFuncCallTree(op2))
				throw std::exception("Function call in comparison is invalid!\n");
			typeOp1 = scopeTree->getFunction(op2->getChild(1)->getToken()->getTokenData())->getReturnType();
		}
		if (op1->getNonTerminalType() != _FUNC_CALL)
		{
			if (op1->getToken()->getType() == IDENTIFIER)
			{
				if ((identifier = scopeTree->checkForIdentifier(op1->getToken())) == nullptr)
					throw std::exception("Use of non existent identifier in comparison!\n");
				typeOp1 = identifier->getType();
			}
			else
				typeOp1 = getIdentifierTypeFromTreeNode(op1);
		}
		if(op2->getNonTerminalType() != _FUNC_CALL)
		{
			if (op2->getToken()->getType() == IDENTIFIER)
			{
				if ((identifier = scopeTree->checkForIdentifier(op2->getToken())) == nullptr)
					throw std::exception("Use of non existent identifier in comparison!\n");
				typeOp2 = identifier->getType();
			}
			else
				typeOp2 = getIdentifierTypeFromTreeNode(op2);
		}
		if (typeOp1 != typeOp2)
			throw std::exception("Type mismatch in comparison!\n");
	}
	catch (std::exception e)
	{
		semanticErrorHandler->handleSemanticError(e);
	}
}

//checks return statement validity.
void SemanticAnalyzer::CheckRet()
{
	try {
		if(!scopeTree->getCurrFunc())
			throw std::exception("Function of type void (main function) cannot return a value!\n");
		if (scopeTree->getCurrFunc()->getReturnType() == VOID_TYPE)
			throw std::exception("Function of type void cannot return a value!\n");
		Token* t;
		Symbol* identifier;
		TreeNode* returnableNode = parserPointer->getStack()->top().first->getChild(1);
	
		if ((t = returnableNode->getChild(0)->getToken()) != nullptr && t->getType() == IDENTIFIER)
		{
			if ((identifier = scopeTree->checkForIdentifier(t)) == nullptr)
				throw std::exception("Cannot return non existent variable!\n");
			else if(identifier->getType() != scopeTree->getCurrFunc()->getReturnType())
				throw std::exception("Variable type doesn't match function return type!\n");
		}
		else if(getIdentifierTypeFromTreeNode(returnableNode->getChild(0)) != scopeTree->getCurrFunc()->getReturnType())
			throw std::exception("Returned value doesn't match function return type!\n");
		scopeTree->getCurrFunc()->setIsReturnedGood(true);
	}
	catch (std::exception e)
	{
		semanticErrorHandler->handleSemanticError(e);
	}
}


//this function takes a tree node and gets identifier type from it.
identifierType SemanticAnalyzer::getIdentifierTypeFromTreeNode(TreeNode* t)
{
	if (t->getNonTerminalType() == _LIT)
		return TokenType_To_IdentifierType(t->getChild(0)->getToken()->getType());
	return TokenType_To_IdentifierType(t->getToken()->getType());
}

//general function used in the function call checks, it checks whether the function exists, and if the parameters are matching.
bool SemanticAnalyzer::checkFuncCallTree(TreeNode* funcCallRoot)
{
	TreeNode* pTree = funcCallRoot;
	Function* func = scopeTree->getFunction(pTree->getChild(1)->getToken()->getTokenData());
	if (func == nullptr)
		return false;
	if (pTree->getChildren().size() != 5 && func->getAmountOfParameters())
		return false;
	if (pTree->getChildren().size() == 5 && (recursiveGetCallablesSize(pTree->getChild(3))
		!= func->getAmountOfParameters() || !recursiveCheckFuncParameters(0, pTree->getChild(3), func)))
		return false;
	return true;
}

// recursive function to traverse tree and check how many parameters where passed to function
int SemanticAnalyzer::recursiveGetCallablesSize(TreeNode* callablesTree)
{
	if (callablesTree->getChildren().size() != 3)
		return 1;
	return 1 + recursiveGetCallablesSize(callablesTree->getChild(2));
}

//used only when it is known that parameters and callables are same size
bool SemanticAnalyzer::recursiveCheckFuncParameters(int currIndex, TreeNode* callablesTree, Function* func)
{
	if (callablesTree->getChildren().size() != 3)
	{
		if (callablesTree->getChild(0)->getChild(0)->getNonTerminalType() == _FUNC_CALL)
		{
			TreeNode* funcCall = callablesTree->getChild(0)->getChild(0);
			Function* funcCalled = scopeTree->getFunction(funcCall->getChild(1)->getToken()->getTokenData());
			if (funcCalled->getReturnType() != func->getParameterByIndex(currIndex)->getType())
				return false;
		}
		else {
			if (callablesTree->getChild(0)->getChild(0)->getToken()->getType() == IDENTIFIER
				&& (scopeTree->checkForIdentifier(callablesTree->getChild(0)->getChild(0)->getToken()) == nullptr ||
					scopeTree->checkForIdentifier(callablesTree->getChild(0)->getChild(0)->getToken())->getType() != func->getParameterByIndex(currIndex)->getType()))
				return false;
			if (callablesTree->getChild(0)->getChild(0)->getToken()->getType() == IDENTIFIER)
				return scopeTree->checkForIdentifier(callablesTree->getChild(0)->getChild(0)->getToken())->getType() == func->getParameterByIndex(currIndex)->getType();
			return getIdentifierTypeFromTreeNode(callablesTree->getChild(0)->getChild(0)) == func->getParameterByIndex(currIndex)->getType();
		}
	}
	else if (callablesTree->getChild(0)->getChild(0)->getNonTerminalType() == _FUNC_CALL)
	{
		TreeNode* funcCall = callablesTree->getChild(0)->getChild(0);
		Function* funcCalled = scopeTree->getFunction(funcCall->getChild(1)->getToken()->getTokenData());
		if (funcCalled->getReturnType() != func->getParameterByIndex(currIndex)->getType())
			return false;
		return recursiveCheckFuncParameters(currIndex + 1, callablesTree->getChild(2), func);
	}
	else
	{
		if (callablesTree->getChild(0)->getChild(0)->getToken()->getType() == IDENTIFIER
			&& (scopeTree->checkForIdentifier(callablesTree->getChild(0)->getChild(0)->getToken()) == nullptr ||
				scopeTree->checkForIdentifier(callablesTree->getChild(0)->getChild(0)->getToken())->getType() != func->getParameterByIndex(currIndex)->getType()))
			return false;
		if (callablesTree->getChild(0)->getChild(0)->getToken()->getType() != IDENTIFIER)
		{
			return getIdentifierTypeFromTreeNode(callablesTree->getChild(0)->getChild(0)) == func->getParameterByIndex(currIndex)->getType()
				&& recursiveCheckFuncParameters(currIndex + 1, callablesTree->getChild(2), func);
		}
		else
			return recursiveCheckFuncParameters(currIndex + 1, callablesTree->getChild(2), func);
	}
}

void SemanticAnalyzer::CheckFuncCall()// this function checks a function call as a statement, so the return type isn't checked.
{
	TreeNode* funcCallNode = parserPointer->getStack()->top().first->getChild(0);
	try {
		if (!checkFuncCallTree(funcCallNode))
			throw std::exception("Wrong parameters in function call!\n");
	}
	catch (std::exception e)
	{
		semanticErrorHandler->handleSemanticError(e);
	}
}

void SemanticAnalyzer::CheckFuncCallInFactor()// checks function call inside arithmetic expression, checks the return type is int.
{
	string exceptionMessage = ""; // in order to have mutiple combinations of messages.
	TreeNode* funcCallNode = parserPointer->getStack()->top().first->getChild(0);
	Function* func = scopeTree->getFunction(funcCallNode->getChild(1)->getToken()->getTokenData());
	try {
		if (func == nullptr)
			throw std::exception("Function call for non existent function in Arithmetic expression!\n");
		if (func->getReturnType() != INT_TYPE)
			exceptionMessage += "Function call inside Arithmetic expression should return int type!\n";
		if (!checkFuncCallTree(funcCallNode))
			exceptionMessage += "Wrong parameters for function call!\n";
		if (exceptionMessage != "")
			throw std::exception(exceptionMessage.c_str());
	}
	catch (std::exception e)
	{
		semanticErrorHandler->handleSemanticError(e);
	}
}

int SemanticAnalyzer::getAnnouncedInMain()
{
	return announcedInMain;
}

