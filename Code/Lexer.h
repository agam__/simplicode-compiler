#pragma once
#include "Helper.h"
#include "Token.h"
#include "LexerFSM.h"
#include "ErrorHandler.h"
#include <iostream>
#include <vector>
#include <map>

/// <summary>
/// responsible for lexical analysis of the source code string
/// </summary>
class Lexer
{
private:
	std::string sourceCode;
	std::map<State, TokenType> tokenMap;
	LexerFSM* fsm;
	ErrorHandler* lexerErrorHandler;

	/// these function are used in the tokenize function----------------
	// checks if the state fits an operator that can be a part of 2 tokens connected without space
	int isUnexpandableOperatorState(State s);
	// function to add a single token to the tokens vector using the currToken and currState variables in the tokenize function
	void addSingleToken(std::vector<Token>& tokens, string& currToken, State currState);
	/// ----------------------------------------------------------------
public:
	// constructor
	Lexer(const std::string& code, ErrorHandler* errorHandler);
	// this function uses the finite state machine and does the lexical analysis , returning the tokens vector 
	std::vector<Token> tokenize();

};

