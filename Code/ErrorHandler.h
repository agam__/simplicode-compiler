#pragma once
#include <exception>
#include <iostream>
#include <fstream>
using std::cout;
using std::endl;

/// <summary>
/// the Error Handler handles compilation errors, and notifies the user about them via a log file
/// </summary>
class ErrorHandler
{
public:
	ErrorHandler();
	void printParsingErrorMessage();
	void printLexerErrorMessage();
	void handleSemanticError(std::exception e);
	int getAmountOfSemanticErrors();
private:
	void logError(const std::string& errorMessage);

	int amountOfSemanticErrors;
	std::ofstream logFile;
};

