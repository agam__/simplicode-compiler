#pragma once
#include "Helper.h"

/// <summary>
/// represents a production rule of the grammar's syntax
/// </summary>
class ProductionRule
{
public:
	// constructor
	ProductionRule(NonTerminalType non_terminal, int len);
	// getter for LHS field
	NonTerminalType getLHS();
	// getter for RHS_LEN field
	int getRHSlen();
private:
	 NonTerminalType LHS;
	 int RHS_LEN;
};

