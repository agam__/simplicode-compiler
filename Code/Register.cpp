#include "Register.h"

Register::Register(string name, bool isUsed)
{
	registerName = name;
	this->isUsed = isUsed;
}

bool Register::isInUse()
{
	return isUsed;
}

void Register::flipUse()
{
	isUsed = !isUsed;
}

string Register::getName()
{
	return registerName;
}
