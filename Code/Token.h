#pragma once
#include "Helper.h"
#include <iostream>
using std::string;

/// <summary>
/// a token is the most simple building block of the language syntax
/// </summary>
class Token
{
private:
	TokenType tokenType;
	string data;
public:
	// constructor
	Token(TokenType tt, string data);
	// getter function for the tokenType field
	TokenType getType();
	// getter function for the data field
	string getTokenData();
};

