#pragma once
#include "Token.h"
#include <vector>
#include <iostream>

class Register;

/// <summary>
/// this class represents a node in the AST.
/// </summary>
class TreeNode
{
public:
	// constructor
	TreeNode(SymbolType st, int ntt, Token* t, std::vector<TreeNode*> children);
	// setter function for the children field
	void setChildren(std::vector<TreeNode*> children);
	// getter function for the symbolType field
	SymbolType getSymbolType();
	// getter function for the nonTerminalType field
	NonTerminalType getNonTerminalType();
	// getter function for the token field
	Token* getToken();
	// function to access the children of the node via index
	TreeNode* getChild(int index);
	// getter function for the children field
	std::vector<TreeNode*> getChildren();
	// recursive function used to debug and print the AST
	void printTree(int depth);
	// getter function for the holdingRegister field
	Register* getRegister();
	// setter function for the holdingRegister field
	void setRegister(Register* reg);

private:
	SymbolType symbolType;
	int nonTerminalType;
	Token* token;
	std::vector<TreeNode*> children;
	Register* holdingRegister;
};

