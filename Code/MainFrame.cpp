﻿#include "MainFrame.h"
#include "FileDropTarget.h"
#include <wx/stattext.h> 
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/file.h>
#include <cstdlib>

#define VOID wxVOID
#define IN wxIN
#define OUT wxOUT
#define ERROR wxERROR
#define TRUE wxTRUE
#define FALSE wxFALSE
#define INT wxINT
#define BOOL wxBOOL
#define TokenType wxTokenType

#include "Compiler.h"

enum IDs {
    COMPILE_BUTTON_ID = 2,
    WELCOME_MSG_ID = 3,
    CODE_BOX_ID = 4,
    GAUGE_ID = 5,
    FILE_DROP_AREA_ID = 6,
    RUN_BUTTON_ID = 6
};

wxBEGIN_EVENT_TABLE(MainFrame, wxFrame)
    EVT_BUTTON(COMPILE_BUTTON_ID, MainFrame::onCompileButtonClicked)
    EVT_BUTTON(RUN_BUTTON_ID, MainFrame::onRunButtonClicked)
wxEND_EVENT_TABLE()

MainFrame::MainFrame(const wxString& title) : wxFrame(nullptr, wxID_ANY, title){
    wxFont font(12, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
    // Making the main panel of the main window
    wxPanel* panel = new wxPanel(this);
    // Showing the main welcome message of the app
    wxStaticText* welcomeMessage = new wxStaticText(panel, WELCOME_MSG_ID, "Welcome to the SimpliCode Compiler!", wxPoint(250, 65));
    welcomeMessage->SetFont(font);
    //Instructions to notify the user he can drag files
    wxStaticText* dragMsg = new wxStaticText(panel, wxID_ANY, "You can drag files to compile as well", wxPoint(115, 550));
    // Showing the text box that the user will enter code in
    wxTextCtrl* codeBox = new wxTextCtrl(panel, CODE_BOX_ID, "->Your Code Here<", wxPoint(20, 130), wxSize(400, 400), wxTE_MULTILINE | wxTE_LEFT);
    // Button to compile
    wxButton* button = new wxButton(panel, COMPILE_BUTTON_ID, "Compile", wxPoint(550, 250), wxSize(150, 50));

    // Button to run
    wxButton* buttonRun = new wxButton(panel, RUN_BUTTON_ID, "Run", wxPoint(550, 350), wxSize(150, 50));

    // Creating the gauge
    wxGauge* loadCompileGauge = new wxGauge(panel, GAUGE_ID, 100, wxPoint(530, 400), wxSize(200, 25), wxGA_PROGRESS);
    loadCompileGauge->Hide(); // Hide the gauge initially

    codeBox->SetDropTarget(new FileDropTarget(codeBox));
}

void MainFrame::onCompileButtonClicked(wxCommandEvent& event)
{
    // Retrieve the text entered in the text box
    wxTextCtrl* codeBox = wxDynamicCast(FindWindow(CODE_BOX_ID), wxTextCtrl);
    wxGauge* loadCompileGauge = wxDynamicCast(FindWindow(GAUGE_ID), wxGauge);
    wxString code = codeBox->GetValue();

    // Convert wxString to std::string
    std::string codeStdString = code.ToStdString();
    Compiler* c = new Compiler(codeStdString);

    // Compile the code
    std::string compiledCode = c->compile();

    loadCompileGauge->Show();
    // Simulate loading by filling the gauge gradually
    for (int i = 0; i <= 100; ++i) {
        loadCompileGauge->SetValue(i);
        //wxMilliSleep(25); // Delay to simulate loading
    }
    // Hide the gauge after loading
    loadCompileGauge->Hide();
    // Create a new frame to display the compiled code
    wxFrame* compiledCodeFrame = new wxFrame(NULL, wxID_ANY, "Compiled Code");
    wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
    wxTextCtrl* compiledCodeTextCtrl = new wxTextCtrl(compiledCodeFrame, wxID_ANY, wxString(compiledCode), wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY);
    sizer->Add(compiledCodeTextCtrl, 1, wxEXPAND | wxALL, 5);

    // Add copy button
    wxButton* copyButton = new wxButton(compiledCodeFrame, wxID_ANY, "Copy");
    copyButton->Bind(wxEVT_BUTTON, [compiledCodeTextCtrl](wxCommandEvent& event) {
        compiledCodeTextCtrl->SetSelection(-1, -1);
        compiledCodeTextCtrl->Copy();
        });
    sizer->Add(copyButton, 0, wxALIGN_CENTER | wxALL, 5);

    // Add save button
    wxButton* saveButton = new wxButton(compiledCodeFrame, wxID_ANY, "Save");
    saveButton->Bind(wxEVT_BUTTON, [compiledCode](wxCommandEvent& event) {
        wxFileDialog saveDialog(NULL, "Save Compiled Code", "", "test.asm", "Assembly files (*.asm)|*.asm", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
        if (saveDialog.ShowModal() == wxID_CANCEL)
            return;

        wxString filePath = saveDialog.GetPath();
        
        int i = filePath.size() - 1;
        int last = i;
        wxFile file(filePath, wxFile::write);
        if (!file.IsOpened()) {
            wxMessageBox("Failed to save the file.", "Error", wxICON_ERROR | wxOK);
            return;
        }
        file.Write(wxString(compiledCode));
        file.Close();
        string fileName = "test";
        std::string command = "start cmd /k \"cd C:\\NASM && Build.bat " + fileName + "\"";
        system(command.c_str());
        });
    sizer->Add(saveButton, 0, wxALIGN_CENTER | wxALL, 5);

    compiledCodeFrame->SetSizer(sizer);
    compiledCodeFrame->Layout();
    compiledCodeFrame->Show();

    // Clean up
    delete c;
}

void MainFrame::onRunButtonClicked(wxCommandEvent& event)
{
    bool compilationFailed = false;
    // Retrieve the text entered in the text box
    wxTextCtrl* codeBox = wxDynamicCast(FindWindow(CODE_BOX_ID), wxTextCtrl);
    wxString code = codeBox->GetValue();

    // Convert wxString to std::string
    std::string codeStdString = code.ToStdString();
    Compiler* c = new Compiler(codeStdString);

    // Compile the code
    std::string compiledCode = c->compile();
    if (compiledCode == "Compilaton error - check log" || compiledCode == "Code Generation failed, not enough registers!")
    {
        wxLogMessage(wxString(compiledCode));
        compilationFailed = true;
    }
    if (!compilationFailed)
    {
        wxString filePath = "C:\\NASM\\run.asm";
        wxFile file(filePath, wxFile::write);

        file.Write(wxString(compiledCode));
        file.Close();

        std::string command = "start cmd /k \"cd C:\\NASM && Build.bat run\"";
        system(command.c_str());
    }
}