#pragma once
#include "Helper.h"
#include <iostream>
#include <vector>
using std::string;
using std::vector;

class Symbol // represents regular variables/parameters
{
public:
	//constructor function for a symbol
	Symbol(identifierType type, string idName, int posInFunc, int posInParameters);

	//getter and setter functions for the symbol's type
	identifierType getType();
	void setType(identifierType type);
	
	//getter and setter functions for identifierName
	string getName();
	void setName(string name);

	//getter function for positionInFunc
	int getPosInFunc();
	
	//getter function for positionInParameters
	int getPosInParameters();

private:
	identifierType type;
	string identifierName;
	int positionInFunc;
	int positionInParameters;
};

