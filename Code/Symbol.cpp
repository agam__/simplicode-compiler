#include "Symbol.h"

Symbol::Symbol(identifierType type, string idName, int posInFunc, int posInParameters)
{
	this->type = type;
	this->identifierName = idName;
	this->positionInFunc = posInFunc;
	this->positionInParameters = posInParameters;
}

identifierType Symbol::getType()
{
	return this->type;
}

void Symbol::setType(identifierType type)
{
	this->type = type;
}

string Symbol::getName()
{
	return this->identifierName;
}

void Symbol::setName(string name)
{
	this->identifierName = name;
}

int Symbol::getPosInFunc()
{
	return positionInFunc;
}

int Symbol::getPosInParameters()
{
	return positionInParameters;
}