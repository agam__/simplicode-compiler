#include "Lexer.h"

Lexer::Lexer(const std::string& code, ErrorHandler* errorHandler)
{
    lexerErrorHandler = errorHandler;
    sourceCode = code;
    fsm = new LexerFSM();
    for (int i = 0; i <= NUM_STATES; i++)
        tokenMap[static_cast<State>(i)] = TokenType::INVALID;
    for(int i = 21; i <= 86; i++)
        tokenMap[static_cast<State>(i)] = TokenType::IDENTIFIER;
    tokenMap[START] = INVALID;
    tokenMap[FUNC_FINAL_STATE] = FUNC;
    tokenMap[RET_FINAL_STATE] = RET;
    tokenMap[IF_STATE] = IF;
    tokenMap[IN_STATE] = IN;
    tokenMap[OUT_FINAL_STATE] = OUT;
    tokenMap[DURING_FINAL_STATE] = DURING;
    tokenMap[FOR_FINAL_STATE] = FOR;
    tokenMap[ELSE_FINAL_STATE] = ELSE;
    tokenMap[BOOL_FINAL_STATE] = BOOL;
    tokenMap[VOID_FINAL_STATE] = VOID;
    tokenMap[INT_STATE] = INT;
    tokenMap[STRING_FINAL_STATE] = STRING;
    tokenMap[TRUE_FINAL_STATE] = TRUE;
    tokenMap[FALSE_FINAL_STATE] = FALSE;
    tokenMap[PLUS_STATE] = PLUS;
    tokenMap[MINUS_STATE] = MINUS;
    tokenMap[STAR_STATE] = STAR;
    tokenMap[FORWARD_SLASH] = FORWARD_SLASH_TOKEN;
    tokenMap[MODOLU] = MODOLU_TOKEN;
    tokenMap[BIGGER] = BIGGER_TOKEN;
    tokenMap[SMALLER] = SMALLER_TOKEN;
    tokenMap[BIGGER_EQUALS] = BIGGER_EQUALS_TOKEN;
    tokenMap[SMALLER_EQUALS] = SMALLER_EQUALS_TOKEN;
    tokenMap[OPENING_PARENTHESIS] = OPENING_PARENTHESIS_TOKEN;
    tokenMap[CLOSING_PARENTHESIS] = CLOSING_PARENTHESIS_TOKEN;
    tokenMap[OPENING_CURLY_BRACKETS] = OPENING_CURLY_BRACKETS_TOKEN;
    tokenMap[CLOSING_CURLY_BRACKETS] = CLOSING_CURLY_BRACKETS_TOKEN;
    tokenMap[OPENING_BRACKETS] = OPENING_BRACKETS_TOKEN;
    tokenMap[CLOSING_BRACKETS] = CLOSING_BRACKETS_TOKEN;
    tokenMap[DOT_SIGN] = DOT_SIGN_TOKEN;
    tokenMap[COMMA_SIGN] = COMMA_SIGN_TOKEN;
    tokenMap[ASSIGNMENT_OPERATOR_STATE] = ASSIGNMENT_OPERATOR;
    tokenMap[TWO_EQUALS_STATE] = TWO_EQUALS;
    tokenMap[DOUBLE_AMPRECENT_STATE] = AND;
    tokenMap[DOUBLE_OR_STATE] = OR;
    tokenMap[NOT_EQUALS_STATE] = NOT_EQUALS;
    tokenMap[INT_LITERAL_STATE] = INT_LITERAL;
    tokenMap[STRING_LITERAL_FINAL_STATE] = STRING_LITERAL;
    tokenMap[MAIN_FINAL_STATE] = MAIN_TOKEN;
    tokenMap[FUNCTIONS_FINAL_STATE] = FUNCTIONS_TOKEN;
    tokenMap[BOOLEAN_FINAL_STATE] = BOOLEAN_TOKEN;
    tokenMap[ARITHMETIC_FINAL_STATE] = ARITHMETIC_TOKEN;
    tokenMap[CALL_FINAL_STATE] = CALL_TOKEN;
    tokenMap[ENDLINE_FINAL_STATE] = ENDLINE_TOKEN;
}

int Lexer::isUnexpandableOperatorState(State s)
{
    return ((s == OPENING_PARENTHESIS) || (s == CLOSING_PARENTHESIS) || (s == OPENING_CURLY_BRACKETS) || (s == CLOSING_CURLY_BRACKETS)
        || (s == OPENING_BRACKETS) || (s == CLOSING_BRACKETS) || (s == DOT_SIGN) || (s == COMMA_SIGN));
}

void Lexer::addSingleToken(std::vector<Token>& tokens, string& currToken, State currState)
{
    TokenType tokenType = tokenMap[currState];
    if (tokenType == INVALID) throw std::runtime_error("Invalid Token"); 
    if (currToken != "")
        tokens.emplace_back(Token(tokenType, currToken));
    currToken.clear();
}

std::vector<Token> Lexer::tokenize()
{
    State currentState = START;
    std::vector<Token> tokens;
    std::string currentToken;
    TokenType tokenType;
    try {
        for (char ch : sourceCode) {
            // Process the current character
            State nextState = fsm->get(currentState, ch);
            if (nextState == INVALID_STATE) throw std::exception();
            if (nextState == START && isUnexpandableOperatorState(currentState) || (currentState != START && nextState == START && !isUnexpandableOperatorState(currentState)))
            {
                addSingleToken(tokens, currentToken, currentState);
            }
            if (currentState != START && isUnexpandableOperatorState(nextState))
            {
                tokenType = tokenMap[currentState];
                if (tokenType != INVALID) {
                    if (currentToken != "")
                        tokens.emplace_back(Token(tokenType, currentToken));
                    currentToken.clear();
                    currentToken += ch;
                    tokens.emplace_back(Token(tokenMap[nextState], currentToken));
                    currentToken.clear();
                }
                else throw std::runtime_error("Invalid Token");
            }
            else if (nextState != START)
                currentToken += ch;
            // Update the current state after processing the current character
            currentState = nextState;
        }

        // Handle any remaining token at the end of the input
        if (!currentToken.empty()) {
            addSingleToken(tokens, currentToken, currentState);
        }

        tokens.emplace_back(Token(EOF_TOKEN, ""));
        return tokens;
    }
    catch (std::exception e)
    {
        lexerErrorHandler->printLexerErrorMessage();
        return std::vector<Token>();
    }
}
