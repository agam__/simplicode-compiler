#include "LexerFSM.h"

LexerFSM::LexerFSM(){
    initializeTable();
}

State LexerFSM::get(State state, char ch)
{
    return transitionTable[state][static_cast<unsigned char>(ch)];
}

void LexerFSM::initializeAlphaNumToIdentifer(State state)
{
    for (int i = 'a'; i <= 'z'; i++)
        transitionTable[state][i] = IDENTIFIER_STATE;
    for (int i = 'A'; i <= 'Z'; i++)
        transitionTable[state][i] = IDENTIFIER_STATE;

    for (int i = '0'; i <= '9'; i++)
        transitionTable[state][i] = IDENTIFIER_STATE;
    transitionTable[state]['_'] = IDENTIFIER_STATE;
}

void LexerFSM::initializeCommonState(State state)
{
    initializeAlphaNumToIdentifer(state);

    transitionTable[state][' '] = START;

    transitionTable[state]['('] = OPENING_PARENTHESIS;
    transitionTable[state][')'] = CLOSING_PARENTHESIS;
    transitionTable[state]['.'] = DOT_SIGN;
    transitionTable[state][','] = COMMA_SIGN;
}

void LexerFSM::initializeTable()
{
    //START STATE--------------------------------------------
   //what happens if there is a chance for a keyword after the first letter
    transitionTable[START]['f'] = F_STATE;
    transitionTable[START]['r'] = RET1_STATE;
    transitionTable[START]['i'] = I_STATE;
    transitionTable[START]['o'] = OUT1_STATE;
    transitionTable[START]['d'] = DURING1_STATE;
    transitionTable[START]['e'] = ELSE1_STATE;
    transitionTable[START]['b'] = BOOL1_STATE;
    transitionTable[START]['v'] = VOID1_STATE;
    transitionTable[START]['s'] = STRING1_STATE;
    transitionTable[START]['t'] = TRUE1_STATE;
    transitionTable[START]['m'] = MAIN1_STATE;
    transitionTable[START]['A'] = ARITHMETIC1_STATE;
    transitionTable[START]['B'] = BOOLEAN1_STATE;
    transitionTable[START]['c'] = CALL1_STATE;

    // making the cols for the rest of the letters point to the identifier state.
    for (char i = 'a'; i <= 'z'; i++)
    {
        if (keyword_starts.find(i) == std::string::npos)
            transitionTable[START][i] = IDENTIFIER_STATE;
    }
    for (char i = 'A'; i <= 'Z'; i++)
    {
        if (keyword_starts.find(i) == std::string::npos)
            transitionTable[START][i] = IDENTIFIER_STATE;
    }

    transitionTable[START]['_'] = IDENTIFIER_STATE;
    transitionTable[START][' '] = START;
    transitionTable[START]['\n'] = START;
    transitionTable[START]['#'] = START;
    transitionTable[START]['\t'] = START;

    for (char i = '0'; i <= '9'; i++)
        transitionTable[START][i] = INT_LITERAL_STATE;
    transitionTable[START]['"'] = STRING_LITERAL_STATE;

    transitionTable[START]['!'] = NOT_STATE;
    transitionTable[START]['+'] = PLUS_STATE;
    transitionTable[START]['-'] = MINUS_STATE;
    transitionTable[START]['/'] = FORWARD_SLASH;
    transitionTable[START]['*'] = STAR_STATE;
    transitionTable[START]['%'] = MODOLU;
    transitionTable[START]['>'] = BIGGER;
    transitionTable[START]['<'] = SMALLER;
    transitionTable[START]['='] = EQUAL_SIGN_STATE;
    transitionTable[START]['&'] = AMPRECENT_STATE;
    transitionTable[START]['|'] = OR_STATE;
    transitionTable[START]['('] = OPENING_PARENTHESIS;
    transitionTable[START][')'] = CLOSING_PARENTHESIS;
    transitionTable[START]['{'] = OPENING_CURLY_BRACKETS;
    transitionTable[START]['}'] = CLOSING_CURLY_BRACKETS;
    transitionTable[START]['['] = OPENING_BRACKETS;
    transitionTable[START][']'] = CLOSING_BRACKETS;
    transitionTable[START]['.'] = DOT_SIGN;
    transitionTable[START][','] = COMMA_SIGN;
    //FINAL STATES
    for (int i = 2; i <= 20; i++)
        initializeAlphaNumToIdentifer(static_cast<State>(i));
    //initializing common states
    for (int i = 21; i <= 86; i++)
        initializeCommonState(static_cast<State>(i));
    //START STATE--------------------------------------------
    // 
    //I_STATE------------------------------------------------
    transitionTable[I_STATE]['n'] = IN_STATE;
    transitionTable[I_STATE]['f'] = IF_STATE;
    //I_STATE------------------------------------------------
    //IN_STATE-----------------------------------------------
    transitionTable[IN_STATE]['t'] = INT_STATE;
    //IN_STATE-----------------------------------------------
    //IF_STATE-----------------------------------------------
    transitionTable[IF_STATE]['('] = OPENING_PARENTHESIS;
    //IF_STATE-----------------------------------------------
    //INT_STATE
    transitionTable[INT_STATE][' '] = START;
    //INT_STATE
    //F_STATE
    transitionTable[F_STATE]['u'] = FUNC2_STATE;
    transitionTable[F_STATE]['a'] = FALSE2_STATE;
    transitionTable[F_STATE]['o'] = FOR2_STATE;
    //F_STATE
    //FUNC2_STATE
    transitionTable[FUNC2_STATE]['n'] = FUNC3_STATE;
    //
    //FUNC3_STATE
    transitionTable[FUNC3_STATE]['c'] = FUNC_FINAL_STATE;
    //FUNC_FINAL_STATE
    transitionTable[FUNC_FINAL_STATE][' '] = START;
    transitionTable[FUNC_FINAL_STATE]['t'] = FUNCTIONS5_STATE;
    //FUNCTIONS5_STATE
    transitionTable[FUNCTIONS5_STATE]['i'] = FUNCTIONS6_STATE;
    //FUNCTIONS6_STATE
    transitionTable[FUNCTIONS6_STATE]['o'] = FUNCTIONS7_STATE;
    //FUNCTIONS7_STATE
    transitionTable[FUNCTIONS7_STATE]['n'] = FUNCTIONS8_STATE;
    //FUNCTIONS8_STATE
    transitionTable[FUNCTIONS8_STATE]['s'] = FUNCTIONS_FINAL_STATE;
    //FUNCTIONS_FINAL_STATE
    transitionTable[FUNCTIONS_FINAL_STATE]['{'] = OPENING_CURLY_BRACKETS;
    transitionTable[FUNCTIONS_FINAL_STATE]['\n'] = START;
    //----------------------
    //FALSE2_STATE
    transitionTable[FALSE2_STATE]['l'] = FALSE3_STATE;
    //----------------------
    //FALSE3_STATE
    transitionTable[FALSE3_STATE]['s'] = FALSE4_STATE;
    //
    //FALSE4_STATE
    transitionTable[FALSE4_STATE]['e'] = FALSE_FINAL_STATE;
    //
    //FALSE_FINAL_STATE
    transitionTable[FALSE_FINAL_STATE][')'] = CLOSING_PARENTHESIS;
    transitionTable[FALSE_FINAL_STATE]['.'] = DOT_SIGN;
    transitionTable[FALSE_FINAL_STATE][' '] = START;
    //
    //IDENTIFIER_STATE
    //
    //RET1_STATE
    transitionTable[RET1_STATE]['e'] = RET2_STATE;
    //
    //RET2_STATE
    transitionTable[RET2_STATE]['t'] = RET_FINAL_STATE;
    //
    //RET_FINAL_STATE
    transitionTable[RET_FINAL_STATE]['.'] = DOT_SIGN;
    transitionTable[RET_FINAL_STATE]['('] = OPENING_PARENTHESIS;
    transitionTable[RET_FINAL_STATE][' '] = START;
    //
    //OUT1_STATE 
    transitionTable[OUT1_STATE]['u'] = OUT2_STATE;
    //
    //OUT2_STATE
    transitionTable[OUT2_STATE]['t'] = OUT_FINAL_STATE;
    //OUT_FINAL_STATE
    transitionTable[OUT_FINAL_STATE]['('] = OPENING_PARENTHESIS;
    //
    //DURING1_STATE
    transitionTable[DURING1_STATE]['u'] = DURING2_STATE;
    //
    //DURING2_STATE
    transitionTable[DURING2_STATE]['r'] = DURING3_STATE;
    //
    //DURING3_STATE
    transitionTable[DURING3_STATE]['i'] = DURING4_STATE;
    //
    //DURING4_STATE
    transitionTable[DURING4_STATE]['n'] = DURING5_STATE;
    //
    //DURING5_STATE 
    transitionTable[DURING5_STATE]['g'] = DURING_FINAL_STATE;
    //
    //DURING_FINAL_STATE
    transitionTable[DURING_FINAL_STATE]['('] = OPENING_PARENTHESIS;
    //
    //FOR2_STATE
    transitionTable[FOR2_STATE]['r'] = FOR_FINAL_STATE;
    //
    //FOR_FINAL_STATE
    transitionTable[FOR_FINAL_STATE]['('] = OPENING_PARENTHESIS;
    //ELSE1_STATE
    transitionTable[ELSE1_STATE]['l'] = ELSE2_STATE;
    transitionTable[ELSE1_STATE]['n'] = ENDLINE2_STATE;
    //ELSE2_STATE
    transitionTable[ELSE2_STATE]['s'] = ELSE3_STATE;
    //ELSE3_STATE
    transitionTable[ELSE3_STATE]['e'] = ELSE_FINAL_STATE;
    //ELSE_FINAL_STATE
    transitionTable[ELSE_FINAL_STATE]['\n'] = START;
    transitionTable[ELSE_FINAL_STATE]['{'] = OPENING_CURLY_BRACKETS;

    //ENDLINE2_STATE
    transitionTable[ENDLINE2_STATE]['d'] = ENDLINE3_STATE;
    //ENDLINE3_STATE
    transitionTable[ENDLINE3_STATE]['L'] = ENDLINE4_STATE;
    //ENDLINE4_STATE
    transitionTable[ENDLINE4_STATE]['i'] = ENDLINE5_STATE;
    //ENDLINE5_STATE
    transitionTable[ENDLINE5_STATE]['n'] = ENDLINE6_STATE;
    //ENDLINE6_STATE
    transitionTable[ENDLINE6_STATE]['e'] = ENDLINE_FINAL_STATE;
    //ENDLINE_FINAL_STATE
    transitionTable[ENDLINE_FINAL_STATE]['('] = OPENING_PARENTHESIS;
    transitionTable[ENDLINE_FINAL_STATE][' '] = START;

    //BOOL1_STATE
    transitionTable[BOOL1_STATE]['o'] = BOOL2_STATE;
    //BOOL2_STATE
    transitionTable[BOOL2_STATE]['o'] = BOOL3_STATE;
    //BOOL3_STATE
    transitionTable[BOOL3_STATE]['l'] = BOOL_FINAL_STATE;
    //BOOL_FINAL_STATE
    transitionTable[BOOL_FINAL_STATE][' '] = START;
    //VOID1_STATE
    transitionTable[VOID1_STATE]['o'] = VOID2_STATE;
    //VOID2_STATE
    transitionTable[VOID2_STATE]['i'] = VOID3_STATE;
    //VOID3_STATE
    transitionTable[VOID3_STATE]['d'] = VOID_FINAL_STATE;
    //VOID_FINAL_STATE
    transitionTable[VOID_FINAL_STATE][' '] = START;
    //STRING1_STATE
    transitionTable[STRING1_STATE]['t'] = STRING2_STATE;
    //STRING2_STATE
    transitionTable[STRING2_STATE]['r'] = STRING3_STATE;
    //STRING3_STATE
    transitionTable[STRING3_STATE]['i'] = STRING4_STATE;
    //STRING4_STATE
    transitionTable[STRING4_STATE]['n'] = STRING5_STATE;
    //STRING5_STATE 
    transitionTable[STRING5_STATE]['g'] = STRING_FINAL_STATE;
    //STRING_FINAL_STATE
    transitionTable[STRING_FINAL_STATE][' '] = START;
    //TRUE1_STATE
    transitionTable[TRUE1_STATE]['r'] = TRUE2_STATE;
    //TRUE2_STATE
    transitionTable[TRUE2_STATE]['u'] = TRUE3_STATE;
    //TRUE3_STATE
    transitionTable[TRUE3_STATE]['e'] = TRUE_FINAL_STATE;
    //TRUE_FINAL_STATE
    transitionTable[TRUE_FINAL_STATE]['.'] = DOT_SIGN;
    transitionTable[TRUE_FINAL_STATE][')'] = CLOSING_PARENTHESIS;
    transitionTable[TRUE_FINAL_STATE][' '] = START;
    //PLUS_STATE
    transitionTable[PLUS_STATE][' '] = START;
    //MINUS_STATE
    transitionTable[MINUS_STATE][' '] = START;
    //STAR_STATE
    transitionTable[STAR_STATE][' '] = START;
    //FORWARD_SLASH 
    transitionTable[FORWARD_SLASH][' '] = START;
    //MODOLU
    transitionTable[MODOLU][' '] = START;
    //BIGGER
    transitionTable[BIGGER]['='] = BIGGER_EQUALS;
    transitionTable[BIGGER][' '] = START;
    //SMALLER
    transitionTable[SMALLER]['='] = SMALLER_EQUALS;
    transitionTable[SMALLER][' '] = START;
    transitionTable[SMALLER]['-'] = ASSIGNMENT_OPERATOR_STATE;
    //BIGGER_EQUALS
    transitionTable[BIGGER_EQUALS][' '] = START;
    //SMALLER_EQUALS
    transitionTable[SMALLER_EQUALS][' '] = START;
    //OPENING_PARENTHESIS
    initializeAlphaNumToIdentifer(OPENING_PARENTHESIS);
    for (char ch = '0'; ch <= '9'; ch++)
        transitionTable[OPENING_PARENTHESIS][ch] = INT_LITERAL_STATE;
    transitionTable[OPENING_PARENTHESIS]['f'] = F_STATE;
    transitionTable[OPENING_PARENTHESIS]['r'] = RET1_STATE;
    transitionTable[OPENING_PARENTHESIS]['i'] = I_STATE;
    transitionTable[OPENING_PARENTHESIS]['o'] = OUT1_STATE;
    transitionTable[OPENING_PARENTHESIS]['d'] = DURING1_STATE;
    transitionTable[OPENING_PARENTHESIS]['e'] = ELSE1_STATE;
    transitionTable[OPENING_PARENTHESIS]['b'] = BOOL1_STATE;
    transitionTable[OPENING_PARENTHESIS]['v'] = VOID1_STATE;
    transitionTable[OPENING_PARENTHESIS]['s'] = STRING1_STATE;
    transitionTable[OPENING_PARENTHESIS]['t'] = TRUE1_STATE;
    transitionTable[OPENING_PARENTHESIS][' '] = START;
    transitionTable[OPENING_PARENTHESIS][')'] = CLOSING_PARENTHESIS;
    transitionTable[OPENING_PARENTHESIS]['('] = OPENING_PARENTHESIS;
    transitionTable[OPENING_PARENTHESIS]['"'] = STRING_LITERAL_STATE;
    transitionTable[OPENING_PARENTHESIS]['c'] = CALL1_STATE;
    //CLOSING_PARENTHESIS
    transitionTable[CLOSING_PARENTHESIS][')'] = CLOSING_PARENTHESIS;
    transitionTable[CLOSING_PARENTHESIS][' '] = START;
    transitionTable[CLOSING_PARENTHESIS]['.'] = DOT_SIGN;
    transitionTable[CLOSING_PARENTHESIS]['{'] = OPENING_CURLY_BRACKETS;
    transitionTable[CLOSING_PARENTHESIS]['\n'] = START;
    //OPENING_CURLY_BRACKETS
    transitionTable[OPENING_CURLY_BRACKETS]['f'] = F_STATE;
    transitionTable[OPENING_CURLY_BRACKETS]['r'] = RET1_STATE;
    transitionTable[OPENING_CURLY_BRACKETS]['i'] = I_STATE;
    transitionTable[OPENING_CURLY_BRACKETS]['o'] = OUT1_STATE;
    transitionTable[OPENING_CURLY_BRACKETS]['d'] = DURING1_STATE;
    transitionTable[OPENING_CURLY_BRACKETS]['b'] = BOOL1_STATE;
    transitionTable[OPENING_CURLY_BRACKETS]['s'] = STRING1_STATE;
    transitionTable[OPENING_CURLY_BRACKETS][' '] = START;
    transitionTable[OPENING_CURLY_BRACKETS]['\n'] = START;
    //CLOSING_CURLY_BRACKETS
    transitionTable[CLOSING_CURLY_BRACKETS][' '] = START;
    transitionTable[CLOSING_CURLY_BRACKETS]['\n'] = START;
    //DOT_SIGN
    transitionTable[DOT_SIGN][' '] = START;
    transitionTable[DOT_SIGN]['\n'] = START;
    transitionTable[DOT_SIGN]['}'] = CLOSING_CURLY_BRACKETS;
    //COMMA_SIGN
    transitionTable[COMMA_SIGN][' '] = START;
    //ASSIGNMENT_OPERATOR_STATE
    transitionTable[ASSIGNMENT_OPERATOR_STATE][' '] = START;
    //INT_LITERAL_STATE
    transitionTable[INT_LITERAL_STATE][' '] = START;
    for (int i = '0'; i <= '9'; i++)
        transitionTable[INT_LITERAL_STATE][i] = INT_LITERAL_STATE;
    transitionTable[INT_LITERAL_STATE][')'] = CLOSING_PARENTHESIS;
    transitionTable[INT_LITERAL_STATE]['.'] = DOT_SIGN;
    transitionTable[INT_LITERAL_STATE][','] = COMMA_SIGN;
    //STRING_LITERAL_STATE
    for (int i = 'a'; i <= 'z'; i++)
        transitionTable[STRING_LITERAL_STATE][i] = STRING_LITERAL_STATE;
    for (int i = 'A'; i <= 'Z'; i++)
        transitionTable[STRING_LITERAL_STATE][i] = STRING_LITERAL_STATE;

    for (int i = '0'; i <= '9'; i++)
        transitionTable[STRING_LITERAL_STATE][i] = STRING_LITERAL_STATE;
    transitionTable[STRING_LITERAL_STATE]['_'] = STRING_LITERAL_STATE;
    transitionTable[STRING_LITERAL_STATE][' '] = STRING_LITERAL_STATE;
    transitionTable[STRING_LITERAL_STATE]['*'] = STRING_LITERAL_STATE;
    transitionTable[STRING_LITERAL_STATE]['!'] = STRING_LITERAL_STATE;
    transitionTable[STRING_LITERAL_STATE]['"'] = STRING_LITERAL_FINAL_STATE;
    //STRING_LITERAL_FINAL_STATE
    transitionTable[STRING_LITERAL_FINAL_STATE]['.'] = DOT_SIGN;
    transitionTable[STRING_LITERAL_FINAL_STATE][')'] = CLOSING_PARENTHESIS;
    transitionTable[STRING_LITERAL_FINAL_STATE][' '] = START;
    //EQUAL_SIGN_STATE
    transitionTable[EQUAL_SIGN_STATE]['='] = TWO_EQUALS_STATE;
    //TWO_EQUALS_STATE
    transitionTable[TWO_EQUALS_STATE][' '] = START;
    //AMPRECENT_STATE
    transitionTable[AMPRECENT_STATE]['&'] = DOUBLE_AMPRECENT_STATE;
    //DOUBLE_AMPRECENT_STATE
    transitionTable[DOUBLE_AMPRECENT_STATE][' '] = START;
    //OR_STATE
    transitionTable[OR_STATE]['|'] = DOUBLE_OR_STATE;
    //DOUBLE_OR_STATE
    transitionTable[DOUBLE_OR_STATE][' '] = START;
    //NOT_STATE
    transitionTable[NOT_STATE]['='] = NOT_EQUALS_STATE;
    //NOT_EQUALS_STATE
    transitionTable[NOT_EQUALS_STATE][' '] = START;
    //MAIN1_STATE
    transitionTable[MAIN1_STATE]['a'] = MAIN2_STATE;
    //MAIN2_STATE
    transitionTable[MAIN2_STATE]['i'] = MAIN3_STATE;
    //MAIN3_STATE
    transitionTable[MAIN3_STATE]['n'] = MAIN_FINAL_STATE;
    //MAIN_FINAL_STATE
    transitionTable[MAIN_FINAL_STATE]['('] = OPENING_PARENTHESIS;
    //ARITHMETIC1_STATE
    transitionTable[ARITHMETIC1_STATE]['r'] = ARITHMETIC2_STATE;
    //ARITHMETIC2_STATE
    transitionTable[ARITHMETIC2_STATE]['i'] = ARITHMETIC3_STATE;
    //ARITHMETIC3_STATE
    transitionTable[ARITHMETIC3_STATE]['t'] = ARITHMETIC4_STATE;
    //ARITHMETIC4_STATE
    transitionTable[ARITHMETIC4_STATE]['h'] = ARITHMETIC5_STATE;
    //ARITHMETIC5_STATE
    transitionTable[ARITHMETIC5_STATE]['m'] = ARITHMETIC6_STATE;
    //ARITHMETIC6_STATE
    transitionTable[ARITHMETIC6_STATE]['e'] = ARITHMETIC7_STATE;
    //ARITHMETIC7_STATE
    transitionTable[ARITHMETIC7_STATE]['t'] = ARITHMETIC8_STATE;
    //ARITHMETIC8_STATE
    transitionTable[ARITHMETIC8_STATE]['i'] = ARITHMETIC9_STATE;
    //ARITHMETIC9_STATE
    transitionTable[ARITHMETIC9_STATE]['c'] = ARITHMETIC_FINAL_STATE;
    //ARITHMETIC_FINAL_STATE
    transitionTable[ARITHMETIC_FINAL_STATE][' '] = START;
    //BOOLEAN1_STATE
    transitionTable[BOOLEAN1_STATE]['o'] = BOOLEAN2_STATE;
    //BOOLEAN2_STATE
    transitionTable[BOOLEAN2_STATE]['o'] = BOOLEAN3_STATE;
    //BOOLEAN3_STATE
    transitionTable[BOOLEAN3_STATE]['l'] = BOOLEAN4_STATE;
    //BOOLEAN4_STATE
    transitionTable[BOOLEAN4_STATE]['e'] = BOOLEAN5_STATE;
    //BOOLEAN5_STATE
    transitionTable[BOOLEAN5_STATE]['a'] = BOOLEAN6_STATE;
    //BOOLEAN6_STATE
    transitionTable[BOOLEAN6_STATE]['n'] = BOOLEAN_FINAL_STATE;
    //BOOLEAN_FINAL_STATE
    transitionTable[BOOLEAN_FINAL_STATE][' '] = START;
    //CALL1_STATE
    transitionTable[CALL1_STATE]['a'] = CALL2_STATE;
    //CALL2_STATE
    transitionTable[CALL2_STATE]['l'] = CALL3_STATE;
    //CALL3_STATE
    transitionTable[CALL3_STATE]['l'] = CALL_FINAL_STATE;
    //CALL_FINAL_STATE
    transitionTable[CALL_FINAL_STATE][' '] = START;

}