#include "TreeNode.h"
TreeNode::TreeNode(SymbolType st, int ntt, Token* t, std::vector<TreeNode*> children)
{
	this->symbolType = st;
	this->nonTerminalType = ntt;
	this->token = t;
	this->children = children;
	this->holdingRegister = nullptr;
}

void TreeNode::setChildren(std::vector<TreeNode*> children)
{
	this->children = children;
}

SymbolType TreeNode::getSymbolType()
{
	return this->symbolType;
}

NonTerminalType TreeNode::getNonTerminalType()
{
	return (NonTerminalType)this->nonTerminalType;
}

Token* TreeNode::getToken()
{
	return this->token;
}

TreeNode* TreeNode::getChild(int index)
{
	return this->children[index];
}

std::vector<TreeNode*> TreeNode::getChildren()
{
	return this->children;
}

void TreeNode::printTree(int depth) {
	if (this == nullptr)
		return;

	// Print current node
	for (int i = 0; i < depth; ++i)
		std::cout << "  "; // Adjust spacing for better visualization
	if(this->getSymbolType() == NON_TERMINAL)
		std::cout << this->nonTerminalType << std::endl;
	else
		std::cout << this->token->getTokenData() << std::endl;

	// Recursively print children
	for (int i = 0; i < this->children.size(); i++)
		this->getChild(i)->printTree(depth + 1);
		
}

Register* TreeNode::getRegister()
{
	return holdingRegister;
}

void TreeNode::setRegister(Register* reg)
{
	holdingRegister = reg;
}