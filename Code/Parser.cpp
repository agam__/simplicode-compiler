#include "Parser.h"
#include "SemanticAnalyzer.h"

Parser::Parser(ErrorHandler* errorHandler, ScopeTree* scopeTree)
{
	this->parserErrorHandler = errorHandler;
	this->parsingTable = new ParserDPA();
	initProductionRules();
	semanticPointer = new SemanticAnalyzer(this, parserErrorHandler, scopeTree);// this way, the parser has access to the semantic analyzer and the semantic can access the parser
}

stack<pair<TreeNode*, int>>* Parser::getStack()
{
	return &(this->parserStack);
}

void Parser::setTokens(vector<Token> tokens)
{
	this->tokens = tokens;
}

void Parser::initProductionRules()
{
	//rule 0 - START' -> START
	this->productionRules.emplace_back(__START, 1);
	//rule 1 - START -> functions{FUNCS} func void main(){BODY}
	this->productionRules.emplace_back(_START, 12);
	//rule 2 - FUNCS -> FUNC
	this->productionRules.emplace_back(_FUNCS, 1);
	//rule 3 - FUNCS -> FUNC FUNCS
	this->productionRules.emplace_back(_FUNCS, 2);
	//rule 4 - FUNC -> func TYPE identifier(PARAMETERS){BODY}
	this->productionRules.emplace_back(_FUNC, 9);
	//rule 5 - FUNC -> func void identifier(PARAMETERS){BODY}
	this->productionRules.emplace_back(_FUNC, 9);
	//rule 6 - PARAMETERS -> PARAMETER
	this->productionRules.emplace_back(_PARAMETERS, 1);
	//rule 7 - PARAMETERS -> PARAMETER, PARAMETERS
	this->productionRules.emplace_back(_PARAMETERS, 3);
	//rule 8 - PARAMETER -> TYPE identifier
	this->productionRules.emplace_back(_PARAMETER, 2);
	//rule 9 - TYPE -> int
	this->productionRules.emplace_back(_TYPE, 1);
	//rule 10 - TYPE -> bool
	this->productionRules.emplace_back(_TYPE, 1);
	//rule 11 - TYPE -> string
	this->productionRules.emplace_back(_TYPE, 1);
	//rule 12 - BODY -> STATEMENTS
	this->productionRules.emplace_back(_BODY, 1);
	//rule 13 - STATEMENTS -> STATEMENT
	this->productionRules.emplace_back(_STATEMENTS, 1);
	//rule 14 - STATEMENTS -> STATEMENT STATEMENTS
	this->productionRules.emplace_back(_STATEMENTS, 2);
	//rule 15 - STATEMENT -> ANNOUNCE
	this->productionRules.emplace_back(_STATEMENT, 1);
	//rule 16 - STATEMENT -> DURINGSTATEMENT
	this->productionRules.emplace_back(_STATEMENT, 1);
	//rule 17 - STATEMENT -> IFFULL
	this->productionRules.emplace_back(_STATEMENT, 1);
	//rule 18 - STATEMENT -> OUTSTATEMENT
	this->productionRules.emplace_back(_STATEMENT, 1);
	//rule 19 - STATEMENT -> INSTATEMENT
	this->productionRules.emplace_back(_STATEMENT, 1);
	//rule 20 - STATEMENT -> FUNCCALL .
	this->productionRules.emplace_back(_STATEMENT, 2);
	//rule 21 - STATEMENT -> ASSIGN
	this->productionRules.emplace_back(_STATEMENT, 1);
	//rule 22 - STATEMENT -> RET
	this->productionRules.emplace_back(_STATEMENT, 1);
	//rule 23 - STATEMENT -> ENDLINE
	this->productionRules.emplace_back(_STATEMENT, 1);
	//rule 24 - RET -> ret RETURNABLE .
	this->productionRules.emplace_back(_RET, 3);
	//rule 25 - RETURNABLE -> identifier
	this->productionRules.emplace_back(_RETURNABLE, 1);
	//rule 26 - RETURNABLE -> LIT
	this->productionRules.emplace_back(_RETURNABLE, 1);
	//rule 27 - RETURNABLE -> Arithmetic EXPRESSION
	this->productionRules.emplace_back(_RETURNABLE, 2);
	//rule 28 - RETURNABLE -> Boolean BOOLEANEXPRESSION
	this->productionRules.emplace_back(_RETURNABLE, 2);
	//rule 29 - ASSIGN -> identifier <- LIT .
	this->productionRules.emplace_back(_ASSIGN, 4);
	//rule 30 - ASSIGN -> identifier <- Arithmetic EXPRESSION .
	this->productionRules.emplace_back(_ASSIGN, 5);
	//rule 31 - ASSIGN -> identifier <- Boolean BOOLEANEXPRESSION .
	this->productionRules.emplace_back(_ASSIGN, 5);
	//rule 32 - ANNOUNCE -> TYPE ASSIGN
	this->productionRules.emplace_back(_ANNOUNCE, 2);
	//rule 33 - LIT -> intlit
	this->productionRules.emplace_back(_LIT, 1);
	//rule 34 - LIT -> strlit
	this->productionRules.emplace_back(_LIT, 1);
	//rule 35 - LIT -> true
	this->productionRules.emplace_back(_LIT, 1);
	//rule 36 - LIT -> false
	this->productionRules.emplace_back(_LIT, 1);
	//rule 37 - EXPRESSION -> TERM + EXPRESSION
	this->productionRules.emplace_back(_EXPRESSION, 3);
	//rule 38 - EXPRESSION -> TERM - EXPRESSION
	this->productionRules.emplace_back(_EXPRESSION, 3);
	//rule 39 - EXPRESSION -> TERM
	this->productionRules.emplace_back(_EXPRESSION, 1);
	//rule 40 - TERM -> FACTOR * TERM
	this->productionRules.emplace_back(_TERM, 3);
	//rule 41 - TERM -> FACTOR / TERM
	this->productionRules.emplace_back(_TERM, 3);
	//rule 42 - TERM -> FACTOR % TERM
	this->productionRules.emplace_back(_TERM, 3);
	//rule 43 - TERM -> FACTOR
	this->productionRules.emplace_back(_TERM, 1);
	//rule 44 - FACTOR -> intlit
	this->productionRules.emplace_back(_FACTOR, 1);
	//rule 45 - FACTOR -> ( EXPRESSION )
	this->productionRules.emplace_back(_FACTOR, 3);
	//rule 46 - FACTOR -> FUNCCALL
	this->productionRules.emplace_back(_FACTOR, 1);
	//rule 47 - FACTOR -> identifier
	this->productionRules.emplace_back(_FACTOR, 1);
	//rule 48 - DURINGSTATEMENT -> during ( BOOLEANEXPRESSION ) { BODY }
	this->productionRules.emplace_back(_DURING_STATEMENT, 7);
	//rule 49 - BOOLEANEXPRESSION -> BOOLEANTERM || BOOLEANEXPRESSION
	this->productionRules.emplace_back(_BOOLEAN_EXPRESSION, 3);
	//rule 50 - BOOLEANEXPRESSION -> BOOLEANTERM
	this->productionRules.emplace_back(_BOOLEAN_EXPRESSION, 1);
	//rule 51 - BOOLEANTERM -> BOOLEANFACTOR
	this->productionRules.emplace_back(_BOOLEAN_TERM, 1);
	//rule 52 - BOOLEANTERM -> BOOLEANFACTOR && BOOLEANTERM
	this->productionRules.emplace_back(_BOOLEAN_TERM, 3);
	//rule 53 - BOOLEANFACTOR -> true
	this->productionRules.emplace_back(_BOOLEAN_FACTOR, 1);
	//rule 54 - BOOLEANFACTOR -> false
	this->productionRules.emplace_back(_BOOLEAN_FACTOR, 1);
	//rule 55 - BOOLEANFACTOR -> COMPARISON
	this->productionRules.emplace_back(_BOOLEAN_FACTOR, 1);
	//rule 56 - BOOLEANFACTOR -> ( BOOLEANEXPRESSION )
	this->productionRules.emplace_back(_BOOLEAN_FACTOR, 3);
	//rule 57 - COMPARISON -> OPERAND COMPARISONOPERATOR OPERAND
	this->productionRules.emplace_back(_COMPARISON, 3);
	//rule 58 - OPERAND -> intlit
	this->productionRules.emplace_back(_OPERAND, 1);
	//rule 59 - OPERAND -> identifier
	this->productionRules.emplace_back(_OPERAND, 1);
	//rule 60 - OPERAND -> Arithmetic EXPRESSION
	this->productionRules.emplace_back(_OPERAND, 2);
	//rule 61 - OPERAND -> FUNCCALL
	this->productionRules.emplace_back(_OPERAND, 1);
	//rule 62 - COMPARISONOPERATOR -> ==
	this->productionRules.emplace_back(_COMPARISON_OP, 1);
	//rule 63 - COMPARISONOPERATOR -> !=
	this->productionRules.emplace_back(_COMPARISON_OP, 1);
	//rule 64 - COMPARISONOPERATOR -> <
	this->productionRules.emplace_back(_COMPARISON_OP, 1);
	//rule 65 - COMPARISONOPERATOR -> <=
	this->productionRules.emplace_back(_COMPARISON_OP, 1);
	//rule 66 - COMPARISONOPERATOR -> >
	this->productionRules.emplace_back(_COMPARISON_OP, 1);
	//rule 67 - COMPARISONOPERATOR -> >=
	this->productionRules.emplace_back(_COMPARISON_OP, 1);
	//Rule 68 - IFFULL -> IFSTATEMENT
	this->productionRules.emplace_back(_IFFULL, 1);
	//rule 69 - IFFULL -> IFSTATEMENT ELSE
	this->productionRules.emplace_back(_IFFULL, 2);
	//rule 70 - IFSTATEMENT -> if ( BOOLEANEXPRESSION ) { BODY }
	this->productionRules.emplace_back(_IF_STATEMENT, 7);
	//rule 71 - ELSE -> else { BODY }
	this->productionRules.emplace_back(_ELSE, 4);
	//rule 72 - OUTSTATEMENT -> out ( PRINTABLE ) .
	this->productionRules.emplace_back(_OUT_STATEMENT, 5);
	//rule 73 - PRINTABLE -> strlit
	this->productionRules.emplace_back(_PRINTABLE, 1);
	//rule 74 - PRINTABLE -> identifier
	this->productionRules.emplace_back(_PRINTABLE, 1);
	//rule 75 - INSTATEMENT -> in ( identifier ) .
	this->productionRules.emplace_back(_IN_STATEMENT, 5);
	//rule 76 - FUNCCALL -> call identifier ( CALLABLES )
	this->productionRules.emplace_back(_FUNC_CALL, 5);
	//rule 77 - FUNCCALL -> call identifier ( )
	this->productionRules.emplace_back(_FUNC_CALL, 4);
	//rule 78 - CALLABLES -> CALLABLE
	this->productionRules.emplace_back(_CALLABLES, 1);
	//rule 79 - CALLABLES -> CALLABLE , CALLABLES
	this->productionRules.emplace_back(_CALLABLES, 3);
	//rule 80 - CALLABLE -> identifier
	this->productionRules.emplace_back(_CALLABLE, 1);
	//rule 81 - CALLABLE -> FUNCCALL // need to remove!
	this->productionRules.emplace_back(_CALLABLE, 1);
	//rule 82 - CALLABLE -> intlit
	this->productionRules.emplace_back(_CALLABLE, 1);
	//rule 83 - CALLABLE -> strlit
	this->productionRules.emplace_back(_CALLABLE, 1);
	//rule 84 - CALLABLE -> Boolean BOOLEANEXPRESSION
	this->productionRules.emplace_back(_CALLABLE, 2);
	//rule 85 - ENDLINE -> endLine ( ) .
	this->productionRules.emplace_back(_ENDLINE, 4);
}

void Parser::shift(int gotoState, Token* t)
{
	vector<TreeNode*> v;
	TreeNode* treeNode = new TreeNode(TERMINAL, -1, t, v);
	parserStack.push(std::make_pair(treeNode, gotoState));
}

void Parser::reduce(int productionRule)
{
	vector<TreeNode*> children;
	ProductionRule prodRule = productionRules[productionRule];
	children.resize(prodRule.getRHSlen());
	for (int i = prodRule.getRHSlen() - 1; i >= 0; i--)
	{
		children[i] = parserStack.top().first;
		parserStack.pop();
	}
		
	TreeNode* treeNode = new TreeNode(NON_TERMINAL, prodRule.getLHS(), nullptr, children);
	parserStack.push(std::make_pair(treeNode, parsingTable->getGoto(parserStack.top().second, prodRule.getLHS())));
}

TreeNode* Parser::parse()
{
	int currState;
	Action currentAction(ERROR, -1);
	int currTokenIndex = 0;
	Token* currToken = &(tokens[currTokenIndex]);
	bool failed = false;

	parserStack.push(std::make_pair(nullptr, 0));

	while (!failed)
	{
		currState = parserStack.top().second;
		currentAction = parsingTable->getAction(currState, currToken->getType());
		switch (currentAction.getActionType())
		{
		case SHIFT:
			this->shift(currentAction.getStateOrRule(), currToken);
			currToken = &(tokens[++currTokenIndex]);
			break;
		case REDUCE:
			this->reduce(currentAction.getStateOrRule());
			break;
		case ACCEPT:
			return parserStack.top().first;
		case ERROR:
			failed = true;
			break;
		}
		if (currentAction.getSemanticCheck() != -1)
			semanticPointer->executeSemanticFunction(currentAction.getSemanticCheck());
	}
	parserErrorHandler->printParsingErrorMessage();
	return nullptr;
}

int Parser::getAnnouncedInMain()
{
	return semanticPointer->getAnnouncedInMain();
}
