#pragma once
#include "Token.h"
#include "SymbolTableNode.h"
#include "Function.h"

/// <summary>
/// scope tree is holding important information about the functions, scopes and identifiers in the code
/// used across the semantic analysis and code generation proccesses.
/// </summary>
class ScopeTree
{
public:
	ScopeTree();
	void addScope();
	void moveUp();
	void moveDown();
	void addIdentifier(string name, identifierType type, int posInFunc, int posInParams);
	Symbol* checkForIdentifier(Token* identifierToken);
	void addFunction(string& name, Function func);
	Function* getFunction(string name);
	Function* getCurrFunc();
	void setCurrFunc(Function* func);
	void printCurrScope();
	void resetScopeTree();
	SymbolTableNode* getCurrScope();

private:
	Function* currFunc;
	SymbolTableNode* headScope;
	SymbolTableNode* currScope; 
	map<string, Function*> functionTable;

	void resetChildIndex(SymbolTableNode* p);
};

