@echo off
IF "%1"=="" (
    echo WRONG SYNTAX: run ^<filename^>
    exit /b 1
)
nasm -fwin32 "%1.asm"
IF ERRORLEVEL 1 (
    echo ERROR: Could not assemble file. Abandoned execution! 
    exit /b 1
)
link /SUBSYSTEM:CONSOLE /ENTRY:main "%1.obj" /LIBPATH:"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\MSVC\14.29.30133\lib\x86" legacy_stdio_definitions.lib vcruntime.lib ucrt.lib
IF ERRORLEVEL 1 (
    echo ERROR: Could not link "%1.obj". Abandoned execution! 
    exit /b 1
)
cls
"%1.exe"
