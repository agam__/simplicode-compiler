#pragma once
#include "Helper.h"
#include "Symbol.h"
#include <iostream>
#include <string>
#include <vector>
#include <map>
using std::string;
using std::vector;
using std::map;

/// <summary>
/// this class represents a function in the input code, it holds various details about the functions in the input code
/// </summary>
class Function
{
public:
	//copy constructor
	Function(Function* func);
	//regular constructor
	Function(string& name, map<string, Symbol*>* params, identifierType returnType, vector<Symbol*>* paramsVec);
	//getter function for the functionName field
	string getName();
	//setter function for the functionName field
	void setName(string& name);
	//setter function for the parameters map
	void setParameters(map<string, Symbol*>* parameters);
	//setter function for the parameters vector
	void setParametersVector(vector<Symbol*>* parametersVec);
	//function to get the parameter in position i
	Symbol* getParameterByIndex(int i);
	//function to search a parameter by name
	Symbol* getParameter(string name);
	//return the amount of parameters the function has
	int getAmountOfParameters();
	//returns the function's return type
	identifierType getReturnType();
	//setter for the isReturnedGood field
	void setIsReturnedGood(bool value);
	//getter for the isReturnedGood field
	bool getIsReturnedGood();
	//getter function for variableSizeBytes
	int getVariableSizeBytes();
	//setter function for variableSizeBytes
	void setVariableSizeBytes(int size);
	//getter function for currAnnounce
	int getCurrAnnounce();
	//increases currAnnounce by 1
	void increaseCurrAnnounce();

private:
	string functionName;
	map<string, Symbol*>* parameters;
	vector<Symbol*>* parametersVector;
	identifierType returnType;
	int variableSizeBytes;
	int currAnnounce;
	bool isReturnedGood;
};

