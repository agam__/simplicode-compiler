#include "CodeGenerator.h"

CodeGenerator::CodeGenerator(TreeNode* ast, ScopeTree* scopeTree, int announcedInMain)
{
	// setting the code generator AST to the AST the parser generated.
	AST = ast;
	// putting the registers in the register vector.
	registers.emplace_back("ebx", false);
	registers.emplace_back("ecx", false);
	registers.emplace_back("esi", false);
	registers.emplace_back("edi", false);
	registers.emplace_back("eax", false);
	registers.emplace_back("edx", false);
	this->scopeTree = scopeTree;
	this->scopeTree->resetScopeTree();

	//making the maps
	//generate statement map
	generateStatementMap[_ANNOUNCE] = &CodeGenerator::generateAnnounce;
	generateStatementMap[_DURING_STATEMENT] = &CodeGenerator::generateDuring;
	generateStatementMap[_IFFULL] = &CodeGenerator::generateIf;
	generateStatementMap[_OUT_STATEMENT] = &CodeGenerator::generateOut;
	generateStatementMap[_IN_STATEMENT] = &CodeGenerator::generateIn;
	generateStatementMap[_FUNC_CALL] = &CodeGenerator::generateFuncCall;
	generateStatementMap[_ASSIGN] = &CodeGenerator::generateAssign;
	generateStatementMap[_RET] = &CodeGenerator::generateRet;
	generateStatementMap[_ENDLINE] = &CodeGenerator::generateEndLine;

	//map to convert tokentype to jmp type

	convertTokenTypeToJmpMap[TWO_EQUALS] = "jne";
	convertTokenTypeToJmpMap[NOT_EQUALS] = "je";
	convertTokenTypeToJmpMap[SMALLER_TOKEN] = "jge";
	convertTokenTypeToJmpMap[SMALLER_EQUALS_TOKEN] = "jg";
	convertTokenTypeToJmpMap[BIGGER_TOKEN] = "jle";
	convertTokenTypeToJmpMap[BIGGER_EQUALS_TOKEN] = "jl";

	//initializing the current label counter
	currLabel = 0;
}

string CodeGenerator::generate()
{
	writeToDestStringData("section .data\n");
	writeToDestStringData("\tformat_string db \"%d\", 0\n");
	writeToDestStringData("\tformat_string_char db \"%c\", 0\n");
	writeToDestStringCode("section .text\n");
	writeToDestStringCode("\tglobal main\n\textern _printf, _scanf\n\n");
	scopeTree->moveDown();// setting up the scope tree current scope to be in the functions scope.
	generateFunctions(AST->getChild(2));
	scopeTree->moveUp();// leaving functions scope

	scopeTree->moveDown();// entering main scope
	generateMain(AST->getChild(10)->getChild(0));
	scopeTree->moveUp();// leaving main scope
	return destStringData + destStringCode;
}

void CodeGenerator::writeToDestStringCode(string line)
{
	destStringCode += line;
}

void CodeGenerator::writeToDestStringData(string line)
{
	destStringData += line;
}

Register* CodeGenerator::getRegister()
{
	for (int i = 0; i < SIZE_REGISTER_ARRAY; i++)
	{
		if (!registers[i].isInUse())
		{
			registers[i].flipUse();// saying the register is now in use
			return &registers[i];
		}
			
	}
	throw std::exception("Code Generation failed, not enough registers!");
}

void CodeGenerator::generateFunctions(TreeNode* funcs)
{
	generateFunction(funcs->getChild(0));
	if (funcs->getChildren().size() == 1)
		return;
	generateFunctions(funcs->getChild(1));
}

void CodeGenerator::generateMain(TreeNode* statementsNode)
{
	//the size of all the variables in the main, also setting the position of every symbol in the function
	sizeVariablesMain = getSizeLocalVariablesBytesInStatements(statementsNode, 0);

	writeToDestStringCode("main:\n");
	//now subtracting from esp register 4 * amount of symbols in the scope
	writeToDestStringCode("\tmov ebp, esp\n\tsub esp, " + std::to_string(sizeVariablesMain) + '\n');

	generateStatements(statementsNode);

	//now freeing the local variables of the main function. 
	writeToDestStringCode("\tadd esp, " + std::to_string(sizeVariablesMain) + '\n');
	writeToDestStringCode("\tret");
}

void CodeGenerator::generateFunction(TreeNode* funcNode)
{
	string popString = "";
	int variablesSize;
	// entering the function's scope
	scopeTree->moveDown();

	//setting the current function in the scopeTree to the function we entered
	string funcName = funcNode->getChild(2)->getToken()->getTokenData();
	scopeTree->setCurrFunc(scopeTree->getFunction(funcName));

	//the size of all the variables in the function, also setting the position of every symbol in the function
	variablesSize = getSizeLocalVariablesBytesInStatements(funcNode->getChild(7)->getChild(0), 0);
	
	//giving the function info about the size of variables, useful in other things
	scopeTree->getCurrFunc()->setVariableSizeBytes(variablesSize);

	//generating the function opening line
	writeToDestStringCode(funcName + ":\n");

	//saving the register values on the stack
	for (int i = 0; i < SIZE_REGISTER_ARRAY; i++)
		if(registers[i].getName() != "eax")
			writeToDestStringCode("\tpush " + registers[i].getName() + '\n');
	//preparing for pops as well
	for (int i = SIZE_REGISTER_ARRAY - 1; i >= 0; i--)
		if (registers[i].getName() != "eax")
			popString += "\tpop " + registers[i].getName() + '\n';

	writeToDestStringCode("\tpush ebp\n\tmov ebp, esp\n");

	//now subtracting from esp register 4 * amount of symbols in the scope
	writeToDestStringCode("\tsub esp, " + std::to_string(variablesSize) + '\n');

	//generating all the statements in the function
	generateStatements(funcNode->getChild(7)->getChild(0));

	//label to jump to after return statement
	writeToDestStringCode(funcName + "_ret:\n");

	//now freeing the local variables of the fucntion. 
	writeToDestStringCode("\tmov esp, ebp\n");

	//popping back the caller's ebp register
	writeToDestStringCode("\tpop ebp\n");

	// popping the pushed registers
	writeToDestStringCode(popString);

	writeToDestStringCode("\tret\n");

	scopeTree->moveUp();
	scopeTree->setCurrFunc(nullptr);
}

void CodeGenerator::generateStatements(TreeNode* statementsNode)
{
	generateStatement(statementsNode->getChild(0));
	if (statementsNode->getChildren().size() == 1)
		return;
	generateStatements(statementsNode->getChild(1));
}

void CodeGenerator::generateStatement(TreeNode* statementNode)
{
	TreeNode* currStatement = statementNode->getChild(0);
	auto func = generateStatementMap.find(currStatement->getNonTerminalType());
	(this->*(func->second))(currStatement);
}

void CodeGenerator::generateAnnounce(TreeNode* announceNode)
{
	generateAssign(announceNode->getChild(1));
}
		

void CodeGenerator::generateDuring(TreeNode* duringNode)
{
	string label1 = generateLabelString();
	string label2 = generateLabelString();
	TreeNode* exp = duringNode->getChild(2);
	TreeNode* loopStatements = duringNode->getChild(5)->getChild(0);
	writeToDestStringCode(label1 + ":\n");
	generateBooleanExpression(exp);
	writeToDestStringCode("\tcmp " + exp->getRegister()->getName() + ", 0\n");
	exp->getRegister()->flipUse();
	writeToDestStringCode("\tje " + label2 + '\n');
	scopeTree->moveDown();
	generateStatements(loopStatements);
	scopeTree->moveUp();
	writeToDestStringCode("\tjmp " + label1 + '\n');
	writeToDestStringCode(label2 + ":\n");
}

void CodeGenerator::generateIf(TreeNode* IfNode)
{
	//only if, without else - IFFULL -> IFSTATEMENT
	if (IfNode->getChildren().size() == 1)
	{
		string label = generateLabelString();
		TreeNode* ifStatement = IfNode->getChild(0);
		TreeNode* exp = ifStatement->getChild(2);
		TreeNode* innerStatements = ifStatement->getChild(5)->getChild(0);
		generateBooleanExpression(exp);
		writeToDestStringCode("\tcmp " + exp->getRegister()->getName() + ", 0\n");
		exp->getRegister()->flipUse();
		writeToDestStringCode("\tje " + label + '\n');
		scopeTree->moveDown();
		generateStatements(innerStatements);
		scopeTree->moveUp();
		writeToDestStringCode(label + ":\n");
	}
	// IFFULL -> IFSTATEMENT ELSE
	else
	{
		string label1 = generateLabelString();
		string label2 = generateLabelString();
		TreeNode* ifStatement = IfNode->getChild(0);
		TreeNode* elseStatement = IfNode->getChild(1);
		TreeNode* exp = ifStatement->getChild(2);
		TreeNode* innerStatementsIf = ifStatement->getChild(5)->getChild(0);
		TreeNode* innerStatementElse = elseStatement->getChild(2)->getChild(0);
		generateBooleanExpression(exp);
		writeToDestStringCode("\tcmp " + exp->getRegister()->getName() + ", 0\n");
		exp->getRegister()->flipUse();
		writeToDestStringCode("\tje " + label1 + '\n');
		scopeTree->moveDown();
		generateStatements(innerStatementsIf);
		scopeTree->moveUp();
		writeToDestStringCode("\tjmp " + label2 + '\n');
		writeToDestStringCode(label1 + ":\n");
		scopeTree->moveDown();
		generateStatements(innerStatementElse);
		scopeTree->moveUp();
		writeToDestStringCode(label2 + ":\n");
	}
}

void CodeGenerator::generateOut(TreeNode* outNode)
{
	Token* printedToken = outNode->getChild(2)->getChild(0)->getToken();
	if (printedToken->getType() == IDENTIFIER)
	{
		Symbol* printedIdentifier = scopeTree->checkForIdentifier(printedToken);
		if (printedIdentifier->getType() == INT_TYPE)
		{
			writeToDestStringCode("\tpush dword " + getEbpOffsetString(printedIdentifier) + '\n');
			writeToDestStringCode("\tpush dword format_string\n");
			writeToDestStringCode("\tcall _printf\n\tadd esp, 8\n");
		}
		else
		{
			writeToDestStringCode("\tpush dword " + getEbpOffsetString(printedIdentifier) + '\n');
			writeToDestStringCode("\tcall _printf\n\tadd esp, 4\n");
		}
		
	}
	else
	{
		string name = generateLitString();
		writeToDestStringData("\t" + name + " db " + printedToken->getTokenData() + " , 0\n");
		writeToDestStringCode("\tlea eax, " + name + '\n');
		writeToDestStringCode("\tpush eax\n");
		writeToDestStringCode("\tcall _printf\n\tadd esp, 4\n");
	}
}

void CodeGenerator::generateIn(TreeNode* inNode)
{
	Token* identifierToken = inNode->getChild(2)->getToken();
	Symbol* inputIdentifier = scopeTree->checkForIdentifier(identifierToken);
	writeToDestStringCode("\tlea eax, dword " + getEbpOffsetString(inputIdentifier) + '\n');
	writeToDestStringCode("\tpush eax\n");
	writeToDestStringCode("\tpush dword format_string\n");
	writeToDestStringCode("\tcall _scanf\n\tadd esp, 8\n");
	
}

void CodeGenerator::generateEndLine(TreeNode* endLineNode)
{
	writeToDestStringCode("\tpush 10\n");
	writeToDestStringCode("\tpush dword format_string_char\n");
	writeToDestStringCode("\tcall _printf\n\tadd esp, 8\n");
}

void CodeGenerator::generateFuncCall(TreeNode* funcCallNode)
{
	Function* func = scopeTree->getFunction(funcCallNode->getChild(1)->getToken()->getTokenData());
	//checking if function has any parameters
	if (funcCallNode->getChildren().size() == 5)
	{
		//pushing the parameters in the call
		TreeNode* callables = funcCallNode->getChild(3);
		generateCallables(callables);// calling recursive function to push the parameters
		writeToDestStringCode("\tcall " + func->getName() + '\n');
		//cleaning the parameters from the stack
		writeToDestStringCode("\tadd esp, " + std::to_string(func->getAmountOfParameters() * SIZE_OF_STACK_CELL) + '\n');
	}
	else
		writeToDestStringCode("\tcall " + func->getName() + '\n');
}

void CodeGenerator::generateCallables(TreeNode* callables)
{
	generateCallable(callables->getChild(0));
	if (callables->getChildren().size() == 3)
		generateCallables(callables->getChild(2));
}

void CodeGenerator::generateCallable(TreeNode* callable)
{
	TreeNode* called = callable->getChild(0);
	if (called->getNonTerminalType() == _FUNC_CALL)
	{
		generateFuncCall(called);
		writeToDestStringCode("\tpush eax\n");
	}
	else
	{
		Token* token = called->getToken();
		if (token->getType() == IDENTIFIER)
		{
			Symbol* identifier = scopeTree->checkForIdentifier(token);
			string offsetFromEbp = getEbpOffsetString(identifier);
			writeToDestStringCode("\tpush dword " + offsetFromEbp + '\n');
		}
		if (token->getType() == INT_LITERAL)
			writeToDestStringCode("\tpush " + token->getTokenData() + '\n');
		if (token->getType() == STRING_LITERAL)
		{
			string name = generateLitString();
			writeToDestStringData("\t" + name + " db " + token->getTokenData() + " , 0\n");
			writeToDestStringCode("\tlea eax, " + name + '\n');
			writeToDestStringCode("\tpush eax\n");
		}
	}
}

void CodeGenerator::generateAssign(TreeNode* assignNode)
{
	//first, finding the local variable offset from ebp
	int subFromEbp;

	Symbol* announcedIdentifier = scopeTree->checkForIdentifier(assignNode->getChild(0)->getToken());
	string ebpOffset = getEbpOffsetString(announcedIdentifier);

	if (assignNode->getChild(2)->getNonTerminalType() == _LIT)
	{
		string value;
		Token* lit = assignNode->getChild(2)->getChild(0)->getToken();
		if (lit->getType() == STRING_LITERAL)
		{
			writeToDestStringData("\t" + announcedIdentifier->getName() + " db " + lit->getTokenData() + ", 0" + '\n');
			writeToDestStringCode("\tlea eax, " + announcedIdentifier->getName() + '\n');
			value = "eax";
		}
		else if (lit->getType() == INT_LITERAL)
			value = lit->getTokenData();
		else
			value = lit->getType() == TRUE ? '1' : '0';
		writeToDestStringCode("\tmov dword " + ebpOffset + ", " + value + '\n');
	}
	//in case of assigning an expression
	else
	{
		TreeNode* expression = assignNode->getChild(3);
		if (expression->getNonTerminalType() == _EXPRESSION)
			generateExpression(expression);
		else
			generateBooleanExpression(expression);

		writeToDestStringCode("\tmov dword " + ebpOffset + ", " + expression->getRegister()->getName() + '\n');
		expression->getRegister()->flipUse();
	}
}

void CodeGenerator::generateRet(TreeNode* retNode)
{
	TreeNode* returnable = retNode->getChild(1);
	if (returnable->getChild(0)->getNonTerminalType() == _LIT)
	{
		Token* litToken = returnable->getChild(0)->getChild(0)->getToken();
		if (litToken->getType() == INT_LITERAL)
			writeToDestStringCode("\tmov eax, " + litToken->getTokenData() + '\n');
		else if(litToken->getType() == TRUE)
			writeToDestStringCode("\tmov eax, 1\n");
		else if(litToken->getType() == FALSE)
			writeToDestStringCode("\tmov eax, 0\n");
	}
	else
	{
		Token* token = returnable->getChild(0)->getToken();
		if (token->getType() == IDENTIFIER)
		{
			Symbol* identifier = scopeTree->checkForIdentifier(token);
			string offsetEbp = getEbpOffsetString(identifier);
			writeToDestStringCode("\tmov eax, dword " + offsetEbp + '\n');
		}
		if (token->getType() == ARITHMETIC_TOKEN)
		{
			TreeNode* exp = returnable->getChild(1);
			generateExpression(exp);
			writeToDestStringCode("\tmov eax, " + exp->getRegister()->getName() + '\n');
			exp->getRegister()->flipUse();
		}
		if (token->getType() == BOOLEAN_TOKEN)
		{
			TreeNode* boolExp = returnable->getChild(1);
			generateBooleanExpression(boolExp);
			writeToDestStringCode("\tmov eax, " + boolExp->getRegister()->getName() + '\n');
			boolExp->getRegister()->flipUse();
		}
	}
	writeToDestStringCode("\tjmp " + scopeTree->getCurrFunc()->getName() + "_ret\n");
}

// this function will decide the amount to reduce from esp
int CodeGenerator::getSizeLocalVariablesBytesInStatements(TreeNode* statementsNode, int currPos)
{
	int ret = 0;
	if (statementsNode->getChild(0)->getChild(0)->getNonTerminalType() == _IFFULL)
	{
		if (statementsNode->getChild(0)->getChild(0)->getChildren().size() == 2)
		{
			int sizeIf = getSizeLocalVariablesBytesInStatements(statementsNode->getChild(0)->getChild(0)->getChild(0)->getChild(5)->getChild(0), currPos);
			int sizeElse = getSizeLocalVariablesBytesInStatements(statementsNode->getChild(0)->getChild(0)->getChild(1)->getChild(2)->getChild(0), currPos);
			ret = sizeIf + sizeElse;
		}
		else
			ret = getSizeLocalVariablesBytesInStatements(statementsNode->getChild(0)->getChild(0)->getChild(0)->getChild(5)->getChild(0), currPos);
	}
	else if (statementsNode->getChild(0)->getChild(0)->getNonTerminalType() == _DURING_STATEMENT)
		ret = getSizeLocalVariablesBytesInStatements(statementsNode->getChild(0)->getChild(0)->getChild(5)->getChild(0), currPos);
	else if (statementsNode->getChild(0)->getChild(0)->getNonTerminalType() == _ANNOUNCE)
		ret = 4;
	if (statementsNode->getChildren().size() == 2)
		return ret + getSizeLocalVariablesBytesInStatements(statementsNode->getChild(1), currPos);
	return ret;
}

void CodeGenerator::generateBooleanExpression(TreeNode* expression)
{
	//allocating a register for the boolean expression
	expression->setRegister(getRegister());
	//BOOLEANEXPRESSION -> BOOLEANTERM
	if (expression->getChildren().size() == 1)
	{
		TreeNode* booleanTerm = expression->getChild(0);
		generateBooleanTerm(booleanTerm);
		writeToDestStringCode("\tmov " + expression->getRegister()->getName() + ", " + booleanTerm->getRegister()->getName() + '\n');
		booleanTerm->getRegister()->flipUse();//freeing the term register
	}
	//BOOLEANEXPRESSION -> BOOLEANTERM || BOOLEANEXPRESSION
	else
	{
		TreeNode* booleamTerm = expression->getChild(0);
		generateBooleanTerm(booleamTerm);
		writeToDestStringCode("\tmov " + expression->getRegister()->getName() + ", " + booleamTerm->getRegister()->getName() + '\n');
		booleamTerm->getRegister()->flipUse();

		TreeNode* exp = expression->getChild(2);
		generateBooleanExpression(exp);
		writeToDestStringCode("\tor " + expression->getRegister()->getName() + ", " + exp->getRegister()->getName() + '\n');
		exp->getRegister()->flipUse();
	}
}

void CodeGenerator::generateBooleanTerm(TreeNode* boolTerm)
{
	// allocating register for the boolean term
	boolTerm->setRegister(getRegister());
	//BOOLEANTERM -> BOOLEANFACTOR
	if (boolTerm->getChildren().size() == 1)
	{
		TreeNode* booleanFactor = boolTerm->getChild(0);
		generateBooleanFactor(booleanFactor);
		writeToDestStringCode("\tmov " + boolTerm->getRegister()->getName() + ", "
			+ booleanFactor->getRegister()->getName() + '\n');
		booleanFactor->getRegister()->flipUse();
	}
	//BOOLEANTERM -> BOOLEANFACTOR && BOOLEANTERM
	else
	{
		TreeNode* booleanFactor = boolTerm->getChild(0);
		generateBooleanFactor(booleanFactor);
		writeToDestStringCode("\tmov " + boolTerm->getRegister()->getName() + ", "
			+ booleanFactor->getRegister()->getName() + '\n');
		booleanFactor->getRegister()->flipUse();

		TreeNode* innerTerm = boolTerm->getChild(2);
		generateBooleanTerm(innerTerm);
		writeToDestStringCode("\tand " + boolTerm->getRegister()->getName() + ", "
			+ innerTerm->getRegister()->getName() + '\n');
		innerTerm->getRegister()->flipUse();
	}
}

void CodeGenerator::generateBooleanFactor(TreeNode* boolFactor)
{
	// allocating register for the boolean factor
	boolFactor->setRegister(getRegister());
	if (boolFactor->getChildren().size() == 1)
	{
		if (boolFactor->getChild(0)->getToken())
		{
			string movToFactor = boolFactor->getChild(0)->getToken()->getType() == TRUE ? "1" : "0";
			writeToDestStringCode("\tmov " + boolFactor->getRegister()->getName() + ", " + movToFactor + '\n');
		}
		else
		{
			TreeNode* comparison = boolFactor->getChild(0);
			generateComparison(comparison);
			writeToDestStringCode("\tmov " + boolFactor->getRegister()->getName() + ", " + comparison->getRegister()->getName() + '\n');
			comparison->getRegister()->flipUse();
		}
	}
	else
	{
		TreeNode* exp = boolFactor->getChild(1);
		generateBooleanExpression(exp);
		writeToDestStringCode("\tmov " + boolFactor->getRegister()->getName() + ", " + exp->getRegister()->getName() + '\n');
		exp->getRegister()->flipUse();
	}
}

void CodeGenerator::generateComparison(TreeNode* comparison)
{
	//the comparison's operands and comparison operator
	TreeNode* op1 = comparison->getChild(0);
	TreeNode* op2 = comparison->getChild(2);
	TokenType compOp = comparison->getChild(1)->getChild(0)->getToken()->getType();
	//allocating register for the comparison
	comparison->setRegister(getRegister());
	//generating first operand and moving into comparison register
	generateOperand(op1);
	writeToDestStringCode("\tmov " + comparison->getRegister()->getName() + ", " + op1->getRegister()->getName() + '\n');
	op1->getRegister()->flipUse();

	//generating second operand and comparing the comparison register(which has the first operand) to the op2 register
	generateOperand(op2);
	writeToDestStringCode("\tcmp " + comparison->getRegister()->getName() + ", " + op2->getRegister()->getName() + '\n');
	op2->getRegister()->flipUse();

	string label1 = generateLabelString();
	string label2 = generateLabelString();

	string jmp = convertTokenTypeToJmpMap[compOp];

	writeToDestStringCode("\t" + jmp + " " + label1 + '\n');//
	writeToDestStringCode("\tmov " + comparison->getRegister()->getName() + ", 1\n");
	writeToDestStringCode("\tjmp " + label2 + '\n');
	writeToDestStringCode(label1 + ":\n");//
	writeToDestStringCode("\tmov " + comparison->getRegister()->getName() + ", 0\n");
	writeToDestStringCode(label2 + ":\n");


}

void CodeGenerator::generateOperand(TreeNode* operand)
{
	//allocating a register for the operand
	operand->setRegister(getRegister());
	Token* token = operand->getChild(0)->getToken();
	if (token)
	{
		if (token->getType() == INT_LITERAL)
			writeToDestStringCode("\tmov " + operand->getRegister()->getName() + ", " + token->getTokenData() + '\n');
		if (token->getType() == IDENTIFIER)
		{
			Symbol* identifer = scopeTree->checkForIdentifier(token);
			writeToDestStringCode("\tmov " + operand->getRegister()->getName() + ", dword " + getEbpOffsetString(identifer) + '\n');
		}
		if (token->getType() == ARITHMETIC_TOKEN)
		{
			TreeNode* exp = operand->getChild(1);
			generateExpression(exp);
			writeToDestStringCode("\tmov " + operand->getRegister()->getName() + ", " + exp->getRegister()->getName() + '\n');
			exp->getRegister()->flipUse();
		}
	}
}

void CodeGenerator::generateExpression(TreeNode* expression)
{
	//allocating a register for the expression
	expression->setRegister(getRegister());
	//EXPRESSION -> TERM
	if (expression->getChildren().size() == 1)
	{
		TreeNode* term = expression->getChild(0);
		generateTerm(term);
		writeToDestStringCode("\tmov " + expression->getRegister()->getName() + ", " + term->getRegister()->getName() + '\n');
		term->getRegister()->flipUse();//freeing the term register
	}
	//EXPRESSION -> TERM + EXPRESSION | TERM - EXPRESSION
	else
	{
		TreeNode* term = expression->getChild(0);
		generateTerm(term);
		writeToDestStringCode("\tmov " + expression->getRegister()->getName() + ", " + term->getRegister()->getName() + '\n');
		term->getRegister()->flipUse();
		TreeNode* exp = expression->getChild(2);
		generateExpression(exp);
		if(expression->getChild(1)->getToken()->getType() == PLUS)
			writeToDestStringCode("\tadd " + expression->getRegister()->getName() + ", " + exp->getRegister()->getName() + '\n');
		else
			writeToDestStringCode("\sub " + expression->getRegister()->getName() + ", " + exp->getRegister()->getName() + '\n');
		exp->getRegister()->flipUse();
	}
}

void CodeGenerator::generateTerm(TreeNode* term)
{
	//allocating a register for the expression
	term->setRegister(getRegister());
	if (term->getChildren().size() == 1)
	{
		TreeNode* factor = term->getChild(0);
		factor->setRegister(getRegister());
		generateFactor(factor);
		writeToDestStringCode("\tmov " + term->getRegister()->getName() + ", " + factor->getRegister()->getName() + '\n');
		factor->getRegister()->flipUse();// freeing factor register
	}
	else
	{
		if (term->getChild(1)->getToken()->getType() != STAR)
		{
			TreeNode* factor = term->getChild(0);
			factor->setRegister(&registers[4]);// need to divide by eax
			registers[4].flipUse();
			generateFactor(factor);
			registers[4].flipUse();
			TreeNode* innerTerm = term->getChild(2);
			generateTerm(innerTerm);
			writeToDestStringCode("\tmov edx, 0\n");
			writeToDestStringCode("\tidiv " + innerTerm->getRegister()->getName() + '\n');
			innerTerm->getRegister()->flipUse();
			if (term->getChild(1)->getToken()->getType() == MODOLU_TOKEN)
				writeToDestStringCode("\tmov " + term->getRegister()->getName() + ", edx" + '\n');
			else
				writeToDestStringCode("\tmov " + term->getRegister()->getName() + ", eax" + '\n');
			factor->getRegister()->flipUse();// freeing factor register
		}
		else
		{
			TreeNode* factor = term->getChild(0);
			factor->setRegister(getRegister());
			generateFactor(factor);
			TreeNode* innerTerm = term->getChild(2);
			generateTerm(innerTerm);
			writeToDestStringCode("\timul " + factor->getRegister()->getName() + ", " + innerTerm->getRegister()->getName() + '\n');
			innerTerm->getRegister()->flipUse();
			writeToDestStringCode("\tmov " + term->getRegister()->getName() + ", " + factor->getRegister()->getName() + '\n');
			factor->getRegister()->flipUse();
		}
	}
}

void CodeGenerator::generateFactor(TreeNode* factor)
{
	if (factor->getChild(0)->getToken() && factor->getChildren().size() == 1)
	{
		Token* token = factor->getChild(0)->getToken();
		if (token->getType() == INT_LITERAL)
			writeToDestStringCode("\tmov " + factor->getRegister()->getName() + ", " + token->getTokenData() + '\n');
		else 
		{
			Symbol* identifier = scopeTree->checkForIdentifier(token);
			writeToDestStringCode("\tmov " + factor->getRegister()->getName() + ", dword " + getEbpOffsetString(identifier) + '\n');
		}
	}
	else if(factor->getChild(0)->getToken())
	{
		TreeNode* exp = factor->getChild(1);
		generateExpression(exp);
		writeToDestStringCode("\tmov " + factor->getRegister()->getName() + ", " + exp->getRegister()->getName() + '\n');
		exp->getRegister()->flipUse();
	}
	else
	{
		generateFuncCall(factor->getChild(0));
		writeToDestStringCode("\tmov " + factor->getRegister()->getName() + ", eax\n");
	}
}

string CodeGenerator::getEbpOffsetString(Symbol* identifier)
{
	if (identifier->getPosInFunc() != -1)
	{
		int subFromEbp;
		if (scopeTree->getCurrFunc())// if the current func in the scope tree is nullptr then it means we are in main
			subFromEbp = scopeTree->getCurrFunc()->getVariableSizeBytes() - SIZE_OF_STACK_CELL * identifier->getPosInFunc();
		else
			subFromEbp = sizeVariablesMain - SIZE_OF_STACK_CELL * identifier->getPosInFunc();
		return "[ebp - " + std::to_string(subFromEbp) + ']';
	}
	//in case the identifier is a parameter
	else
	{
		int addToEbp = 28;// because of pushing all the registers in function start
		addToEbp += identifier->getPosInParameters() * SIZE_OF_STACK_CELL;
		return "[ebp + " + std::to_string(addToEbp) + ']';
	}
}

string CodeGenerator::generateLabelString()
{
	currLabel++;
	return "label_" + std::to_string(currLabel);
}

string CodeGenerator::generateLitString()
{
	currLitString++;
	return "lit_" + std::to_string(currLitString);
}