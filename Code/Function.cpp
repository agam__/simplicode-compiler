#include "Function.h"

Function::Function(Function* func)
{
	this->functionName = func->getName();
	this->parameters = func->parameters;
	this->returnType = func->getReturnType();
	this->isReturnedGood = func->isReturnedGood;
	this->currAnnounce = 0;
}

Function::Function(string& name, map<string, Symbol*>* params, identifierType returnType, vector<Symbol*>* paramsVec)
{
	functionName = name;
	parameters = params;
	this->returnType = returnType;
	parametersVector = paramsVec;
	if(returnType == VOID_TYPE)//if the return type is void, then the function is already set and doesnt need return statement
		isReturnedGood = true;
	else
		isReturnedGood = false;
	
}

void Function::setName(string& name)
{
	this->functionName = name;
}

string Function::getName()
{
	return functionName;
}

identifierType Function::getReturnType()
{
	return returnType;
}

Symbol* Function::getParameter(string name)
{
	Symbol* result = nullptr;
	if (this != nullptr && parameters->count(name) > 0)
		result = parameters->at(name);
	return result;
}

void Function::setParameters(map<string, Symbol*>* parameters)
{
	this->parameters = parameters;
}

void Function::setParametersVector(vector<Symbol*>* parametersVec)
{
	parametersVector = parametersVec;
}

Symbol* Function::getParameterByIndex(int i)
{
	return parametersVector->at(i);
}

int Function::getAmountOfParameters()
{
	return parametersVector->size();
}

void Function::setIsReturnedGood(bool value)
{
	isReturnedGood = value;
}

bool Function::getIsReturnedGood()
{
	return isReturnedGood;
}

int Function::getVariableSizeBytes()
{
	return variableSizeBytes;
}

void Function::setVariableSizeBytes(int size)
{
	variableSizeBytes = size;
}

int Function::getCurrAnnounce()
{
	return currAnnounce;
}

void Function::increaseCurrAnnounce()
{
	currAnnounce++;
}