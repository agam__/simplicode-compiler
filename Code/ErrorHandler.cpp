#include "ErrorHandler.h"

ErrorHandler::ErrorHandler() : amountOfSemanticErrors(0) {
    // Open the log file
    logFile.open("error_log.txt", std::ios::out);
    if (!logFile.is_open()) {
        // If the file cannot be opened, print an error message
        std::cerr << "Error: Unable to open log file!" << std::endl;
    }
}

// Function to print parsing error message
void ErrorHandler::printParsingErrorMessage() {
    std::string errorMessage = "Parsing error occurred! syntax error";
    logError(errorMessage); // Log to file
}

// Function to print lexer error message
void ErrorHandler::printLexerErrorMessage() {
    std::string errorMessage = "Lexer error occurred!";
    logError(errorMessage); // Log to file
}

// Function to handle semantic error
void ErrorHandler::handleSemanticError(std::exception e) {
    std::string errorMessage = "Semantic error occurred: " + std::string(e.what());
    logError(errorMessage); // Log to file
    amountOfSemanticErrors++; // Increment error count
}

// Function to get the amount of semantic errors
int ErrorHandler::getAmountOfSemanticErrors() {
    return amountOfSemanticErrors;
}

// Function to log errors to file
void ErrorHandler::logError(const std::string& errorMessage) {
    if (logFile.is_open()) {
        logFile << errorMessage << std::endl; // Write error message to log file
    }
}