#include "ScopeTree.h"

ScopeTree::ScopeTree()
{
	this->headScope = new SymbolTableNode(nullptr);
	this->currScope = this->headScope;
	currFunc = nullptr;
}

void ScopeTree::addScope()
{
	vector<SymbolTableNode*>* v = currScope->getChildren();
	v->emplace_back(new SymbolTableNode(currScope));
}

void ScopeTree::moveUp()
{
	if (currScope != headScope)
		currScope = currScope->getParent();
}

void ScopeTree::moveDown()
{
	if (currScope->getCurrChildIndex() + 1 < currScope->getChildren()->size())
	{
		currScope->setCurrChildIndex(currScope->getCurrChildIndex() + 1);
		currScope = currScope->getCurrChildObject();
	}
}

Symbol* ScopeTree::checkForIdentifier(Token* identifier)
{
	SymbolTableNode* scope = currScope;
	Symbol* symbol = nullptr;
	bool found = false;
	while (!found && scope != nullptr)
	{
		symbol = scope->getSymbolFromTable(identifier->getTokenData());
		if (symbol != nullptr)
			found = true;
		scope = scope->getParent();
	}
	return symbol ? symbol : currFunc->getParameter(identifier->getTokenData());
}

void ScopeTree::addFunction(string& name, Function func)
{
	functionTable.emplace(name, new Function(&func));
}

Function* ScopeTree::getFunction(string name)
{
	return functionTable[name];
}

void ScopeTree::addIdentifier(string name, identifierType type, int posInFunc, int posInParams)
{
	currScope->insertToSymbolTable(name, type, posInFunc, posInParams);
}

void ScopeTree::printCurrScope()
{
	SymbolTableNode* scope = currScope;
	while (scope != headScope)
	{
		scope->printSymbolTable();
		scope = scope->getParent();
	}
}

Function* ScopeTree::getCurrFunc()
{
	return currFunc;
}

void ScopeTree::setCurrFunc(Function* func)
{
	currFunc = func;
}

void ScopeTree::resetScopeTree()
{
	currFunc = nullptr;
	currScope = headScope;
	SymbolTableNode* p = headScope;
	resetChildIndex(p);
}

void ScopeTree::resetChildIndex(SymbolTableNode* p)
{
	if (p == nullptr)
		return;
	p->setCurrChildIndex(-1);
	for (int i = 0; i < p->getChildren()->size(); i++)
		resetChildIndex(p->getChildren()->at(i));
}

SymbolTableNode* ScopeTree::getCurrScope()
{
	return currScope;
}