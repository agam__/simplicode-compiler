#pragma once
#include "Helper.h"
#include <iostream>

using std::string;

/// <summary>
/// the finite state machine of the lexer
/// </summary>
class LexerFSM
{
public:
	// constructor
	LexerFSM();
	// function to access a cell in the Adjacency matrix
	State get(State state, char ch);
private:
	// functions to initialize the Adjacency matrix
	void initializeCommonState(State state);
	void initializeTable();
	void initializeAlphaNumToIdentifer(State state);

	State transitionTable[NUM_STATES][127] = { INVALID_STATE };
	string keyword_starts = "friodebvstmABc";
};

