#ifndef FILE_DROP_TARGET_H
#define FILE_DROP_TARGET_H

#include <wx/wx.h>
#include <wx/dnd.h>

/// <summary>
/// GUI class, used for cases where the user want to drag and drop a code file into the interface
/// </summary>
class FileDropTarget : public wxFileDropTarget {
public:
    FileDropTarget(wxTextCtrl* textCtrl);

    virtual bool OnDropFiles(wxCoord x, wxCoord y, const wxArrayString& filenames) override;

private:
    wxTextCtrl* m_textCtrl;
};

#endif // FILE_DROP_TARGET_H
