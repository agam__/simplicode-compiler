#pragma once
#include <iostream>
using std::string;

class TreeNode;

/// <summary>
/// this class represents a register in the code generation proccess, 
/// so that the code generator knows which registers are allocated and which ones are free
/// </summary>
class Register
{
public:
	// constructor
	Register(string name, bool isUsed);
	// getter function for the field isUsed
	bool isInUse();
	// if isUsed is true, then it makes it false. if isUsed is false, then it makes it true.
	void flipUse();
	// getter function for the field registerName
	string getName();
private:
	string registerName;
	bool isUsed;
};

