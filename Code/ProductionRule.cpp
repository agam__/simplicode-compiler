#include "ProductionRule.h"

ProductionRule::ProductionRule(NonTerminalType non_terminal, int len)
{
	this->LHS = non_terminal;
	this->RHS_LEN = len;;
}

NonTerminalType ProductionRule::getLHS()
{
	return this->LHS;
}

int ProductionRule::getRHSlen()
{
	return this->RHS_LEN;
}