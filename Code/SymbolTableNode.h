#pragma once
#include "Symbol.h"
#include <iostream>
#include <map>
#include <vector>
using std::map;
using std::string;
using std::vector;

/// <summary>
/// A node in the scope tree, representing a single scope and the symbol table of the scope.
/// </summary>
class SymbolTableNode
{
public:
	// constructor
	SymbolTableNode(SymbolTableNode* parent);
	// getter function for parent field
	SymbolTableNode* getParent();
	// get a symbol by name using the symbol table
	Symbol* getSymbolFromTable(string identifierName);
	//insertion to the symbol table
	void insertToSymbolTable(string identifierName, identifierType type, int posInFunc, int posInParams);
	// returns children[currChild]
	SymbolTableNode* getCurrChildObject();
	// getter function for currChild field
	int getCurrChildIndex();
	// setter function for currChild field
	void setCurrChildIndex(int index);
	// getter function for children field
	vector<SymbolTableNode*>* getChildren();
	// getter function for amountOfSymbols field
	int getAmountOfSymbols();
	// debug function to view the symbol table of the scope
	void printSymbolTable();

private:
	SymbolTableNode* parent;
	map<string, Symbol*> symbolTable;
	vector<SymbolTableNode*> children;
	int amountOfSymbols;
	int currChild;
};

