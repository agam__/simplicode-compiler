Welcome to the SimpliCode Compiler Git repository!
this repository contains the project's code and some cool tests.

הוראות התקנה
wxWidgets
תחילה יש להוריד את הספרייה wxWidgets, מצורף קישור לסרטון שמראה איך ניתן לעשות זאת:
לחץ כאן!
דרישות
בכדי להריץ את הפרויקט בצורה תקינה יש להתקין NASM וlink על כונן C כך שהתיקייה שמכילה את nasm.exe ו-link.exe היא: C:\NASM.
התקנת הפרויקט
כעת אפשר להוריד את קבצי הקוד מהתיקייה Code ב-Git Repository של הפרויקט, יש לקחת את הקובץ Build.bat ולשים אותו בתיקיה C:\NASM.
שינויים בקובץ Build.bat
כדי להתאים את קובץ הבניה למחשב שלכם יש לשנות את השורה שמבצעת את הפעולה link בקובץ Build.bat, מצאו את התיקייה במחשב שלכם שמכילה את הקבצים:
legacy_stdio_definitions.lib
 vcruntime.lib
 ucrt.lib
תיקיה זו כנראה תהיה בכתובת דומה לכתובת שיש בקובץ.
כעת החליפו את התיקיה שציינתי כפרמטר להרצת ה-link בתיקיה שמתאימה למחשב שלכם.
הרצה
כעת, כל מה שנותר הוא לבנות פרויקט חדש ב-Visual Studio עם קבצי הקוד וקבצי ה-CSV בתיקיית הפרויקט, ולהריץ!
